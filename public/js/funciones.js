//lista de palabras para verificar Cedula
var palInc = ["BUEI", "CACA", "CAGA", "CAKA", "COGE", "COJE", "COJO", "FETO", "JOTO", "BUEY", "CACO", "CAGO", "CAKO", "COJA", "COJI", "CULO", "GUEY", "KACA", "KACO", "KAGA", "KAGO", "KOGE", "KOJO", "KAKA", "KULO", "MAME", "MAMO", "MEAR", "MEAS", "MEON", "MION", "MOCO", "MULA", "PEDA", "PEDO", "PENE", "PUTA", "PUTO", "QULO", "RATA", "RUIN"];
var sustPalInc = ["BUEX", "CACX", "CAGX", "CAKX", "COGX", "COJX", "COJX", "FETX", "JOTX", "BUEX", "CACX", "CAGX", "CAKX", "COJX", "COJX", "CULX", "GUEX", "KACX", "KACX", "KAGX", "KAGX", "KOGX", "KOJX", "KAKX", "KULX", "MAMX", "MAMX", "MEAX", "MEAX", "MEOX", "MIOX", "MOCX", "MULX", "PEDX", "PEDX", "PENX", "PUTX", "PUTX", "QULX", "RATX", "RUIX"];
function formatoHora(hora) {
    horas=hora.substring(0,2);
    minutos=hora.substring(2,4);
    nhora=horas+':'+minutos;
    return nhora;
}

function formatoFecha(fecha,tipo) {
    console.log(fecha);
    switch(tipo)
    {
        case 1://Cita
        dia = fecha.substr(6, 2);
        mes = fecha.substr(4, 2);
        ano = fecha.substr(0, 4);
        fecha=dia+'-'+mes+'-'+ano;
        break;
        case 2://Fecha ISO a estandar
        aFecha=fecha.split('-');
        dia=aFecha[2];
        mes=aFecha[1];
        anio=aFecha[0];
        fecha=dia+'-'+mes+'-'+anio;
        break;
        case 3:
        aFecha=fecha.split('-');
        dia=aFecha[0];
        mes=aFecha[1];
        anio=aFecha[2];
        fecha=anio+mes+dia;
        break;
        case 4:
        fecha=fecha.toISOString();
        aFecha=fecha.split('-');
        dia=aFecha[0];
        mes=aFecha[1];
        anio=aFecha[2];
        fecha=anio+mes+dia;
        break;
    }

    return fecha;
}

function validarCedulaTrabajador(cedula,nombre,app,apm,fecha)
{
    cedula=document.getElementById(cedula);
    var valCedula=cedula.value;  
    var aNombres=nombre.split(" ");
    var nombres,cedulaGen,ap_p,ap_m;

    if(app!=undefined){
        var aAp=app.split(" ");
        for(i=0;i<aAp.length;i++)
            switch(aAp[i])
        {
            case "DA":case "DAS":case "DE":case "DEL":case "DER":case "DI":case "DIE":case "DD":case "EL":case "LA":case "LOS":case "LAS":case "MAC":case "MC":case "VAN":case "VON":case "Y":
               /* try{
                    ap_p=aAp[i+1];
                }
                catch(ex){
                    ap_p="X";
                }*/
                break;
                default:
                ap_p=aAp[i];
                break;
            }
        }
        else
            ap_p=apm;
        if(apm!=undefined){
            var aAm=apm.split(" ");
            for(i=0;i<aAm.length;i++)
                switch(aAm[i])
            {
                case "DA":case "DAS":case "DE":case "DEL":case "DER":case "DI":case "DIE":case "DD":case "EL":case "LA":case "LOS":case "LAS":case "MAC":case "MC":case "VAN":case "VON":case "Y":
               /* try{
                    ap_m=aAm[1];
                }
                catch(ex){
                    ap_m="X";
                }*/
                break;
                default:
                ap_m=aAm[i];
                break;
            }
        }
        else
            ap_m="X";
        var cedulaaP="";

        if(ap_p[0]=="Ñ")
            cedulaaP="X";
        else
            cedulaaP=ap_p[0];
        for(var i=1;i<ap_p.length;i++)
        {
            if(esVocal(ap_p[i]))
            {
                cedulaGen=cedulaaP+ap_p[i];
                break;
            }
        }


        cedulaGen=cedulaGen+ap_m[0];
 /*   for(i=0;i<palInc.length;i++)
    {
        if(palInc[i]==cedulaGen)
        {
            cedulaGen=sustPalInc[i];
            break;
        }
    }*/
    var iniCed=valCedula.toString().substring(0,3);
    var cedulaAP=cedulaGen;
    var aFecha=fecha.split("/");
    var fechaCedula=valCedula.toString().substring(4);
    var fechaGen=aFecha[2].substr(2,2)+aFecha[1]+aFecha[0];
    //cedulaGen+=fechaGen;
    if(iniCed==cedulaGen)
    {
        if(fechaCedula==fechaGen)
            return true;
        else
        {
            alert("Fecha Incorrecta de Nacimiento \u00f3 c\u00e9dula incorrecta \n Favor de Verificar");
            return false;        
        }
    }
    else
    {
        if(cedulaAP!=iniCed && fechaCedula!=fechaGen)
            alert("Cedula incorrecta \n Favor de Verificar");
        else
            if(cedulaAP!=iniCed)
                alert("Nombre o Apellidos incorrectos, \n Favor de verificar");
            else
                if(fechaCedula!=fechaGen)
                    alert("Fecha Incorrecta de Nacimiento \u00f3 c\u00e9dula incorrecta \n Favor de Verificar");        
                return false;
            }
        }

        function validarFechaCedula(cedula, fechaNac, tipoCed)
        {

            var ret = false;
            var fechaCed = cedula.toString().substring(4);
            var aFecha = fechaNac.split("/");
            fechaNac = aFecha[2].toString().substring(1) + aFecha[1] + aFecha[0] + "";
            if ((tipoCed == 60 || tipoCed == 61) || (tipoCed == 50 || tipoCed == 51)) {
                if (fechaNac < fechaCed)
                    alert("La fecha de Nacimiento debe ser mayor a la del trabajador");
                else
                    ret = true;
            }
            else {
                if (fechaNac > fechaCed)
                    alert("La fecha de Nacimiento debe ser menor a la del trabajador");
                else
                    ret = true;
            }

            return ret;
        }

        function incapacidad() {
            var derecho = $("#inputDerecho").val();
            var aDer = derecho.split("|");
            switch (aDer[1])
            {
                case "10":
                case "20":
                $("#panel-incapacidad").show('fast');
                break;
                default:
                $("#panel-incapacidad").hide('fast');
            }
        }

        function formatoDia(fecha) {
            dia=fecha.getDate();
            mes=fecha.getMonth()+1;
            anio=fecha.getFullYear();
            if(dia<10){
                dia='0'+dia
            } 
            if(mes<10){
                mes='0'+mes
            } 
            fechaO=dia+'-'+mes+'-'+anio;
            return fechaO;
        }


        function nobackbutton(){

           window.location.hash="no-back-button";
           window.location.hash="Again-No-back-button" //chrome   
           window.onhashchange=function(){window.location.hash="no-back-button";}
        }

        $.extend(
        {
            redirectPost: function(location, args)
            {
                var form = $('<form></form>');
                form.attr("method", "post");
                form.attr("action", location);

                $.each( args, function( key, value ) {
                    var field = $('<input></input>');

                    field.attr("type", "hidden");
                    field.attr("name", key);
                    field.attr("value", value);

                    form.append(field);
                });
                $(form).appendTo('body').submit();
            }
        });       


        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
        