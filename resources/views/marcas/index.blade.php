@extends('app')
@section('Scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			$("#modal-modificar-marcas").on('show.bs.modal', function(event) {
				id=$(event.relatedTarget).data('idmarca');
				$.getJSON('{{ url('marca/') }}/'+id, function(json, textStatus) {
						$("#idmarca").val(json['id']);
						$("#marcaModificar").val(json['marca']);
				});
			});
		});
	</script>
@endsection
@section('content')
	<div class="page-title">
		<div class="title_left">
			<h3>Catalogo de Marcas</h3>
		</div>
		<div class="title_right">
			<a class="btn btn-round btn-success" data-toggle="modal" href='#modal-marcas' data-idmarca="0">Nueva marca</a>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row clearfix">
		<div class="col-md-12 col-xs-12">
			<table class="table table-striped table-condensed table-hover" id="data-table">
				<thead>
					<th>Acciones</th>
					<th>Marca</th>
				</thead>
				<tbody>
				@foreach ($marcas as $row)
					<tr>
						<td>
							<div class="btn-group">
								<a class="btn btn-round btn-primary" data-toggle="modal" href='#modal-modificar-marcas' data-idmarca="{{ $row->id }}">Modificar</a>
								<a href="{{ url('/marcas/destroy/'.$row->id) }}" class="btn btn-round btn-danger">Eliminar</a>
							</div>
						</td>
						<td>{{ $row->marca }}</td>
					</tr>
				@endforeach
					
				
				</tbody>
			</table>
		</div>
	</div>

	<div class="modal fade" id="modal-marcas">
		<div class="modal-dialog">
			<div class="modal-content">
			{!! Form::open(['url'=>'/nueva_marca','method'=>'post','data-parsley-validate','class'=>'form-horizontal']) !!}
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Marca</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="inputMarca" class="col-sm-2 control-label">Marca:</label>
						<div class="col-sm-10">
							<input type="text" name="marca" id="inputMarca" class="form-control" required="required">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-primary">Agregar Marca</button>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-modificar-marcas">
		<div class="modal-dialog">
			<div class="modal-content">
			{!! Form::open(['url'=>'/marca_update','method'=>'post','class'=>'form-horizontal','data-parsley-validate']) !!}
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Marca</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="marcaModificar" class="col-sm-2 control-label">Marca:</label>
						<div class="col-sm-10">
							<input type="text" name="marca" id="marcaModificar" class="form-control" required="required">
							<input type="hidden" name="id" id="idmarca">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-primary">Modificar</button>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop