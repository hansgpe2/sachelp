@extends('app')
@section('Scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$("#modal-otorgante").on('show.bs.modal', function(event) {
			id=$(event.relatedTarget).data('idotorgante');
			if(id==0)
			{
				$("#form-otorgante").attr('action', '{{ url('otorgantes/nuevo') }}');
			}
			else{
				$("#form-otorgante").attr('action', '{{ url('otorgantes/modificar') }}');
				$.getJSON('{{ url('otorgante') }}/'+id, function(json, textStatus) {
						$("#inputNombre").val(json['nombre']);
						$("#inputRazonSocial").val(json['razonSocial']);
						$("#inputRfc").val(json['rfc']);
						$("#inputId").val(json['id']);
				});
			}
		});
	});
</script>
@endsection
@section('content')
<div class="page-title">
	<div class="title_left">
		<h3>Catalogo de Proveedores</h3>
	</div>
	<div class="title_right">
		<a class="btn btn-round btn-success" data-toggle="modal" href='#modal-otorgante' data-idotorgante="0">Nuevo Proveedor</a>
	</div>
</div>
<div class="clearfix"></div>
<div class="row clearfix">
	@if (Session::has('error'))
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Error</strong> {{ Session::get('error') }}
	</div>
	@endif
	@if (Session::has('mensaje'))
	<div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Movimiento realizado</strong> {{ Session::get('mensaje') }}
	</div>
	@endif
</div>
<div class="row clearfix">
	<table class="table table-striped table-condensed" id="data-table">
		<thead>
			<tr>
				<th>Acciones</th>
				<th>Nombre</th>
				<th>Razón Social</th>
				<th>RFC</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($otorgantes as $row)
			<tr>
				<td>
					<div class="btn-group">
						<a href="#modal-otorgante" data-toggle="modal" data-idotorgante="{{ $row->id }}" class="btn btn-round btn-default"><i class="fa fa-edit"></i> Editar</a>
						<a href="{{ url('/otorgantes/eliminar/'.$row->id) }}" class="btn btn-round btn-danger"><i class="fa fa-eraser"></i> Eliminar</a>
					</div>
				</td>
				<td>{{ $row->nombre }}</td>
				<td>{{ $row->razonSocial }}</td>
				<td>{{ $row->rfc }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>


<div class="modal fade" id="modal-otorgante">
	<div class="modal-dialog">
		{!! Form::open(['url'=>'','class'=>'form-horizontal','method'=>'post', 'id'=>'form-otorgante']) !!}
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Datos del Proveedor</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="inputNombre" class="col-sm-2 control-label">Nombre:</label>
					<div class="col-sm-10">
						<input type="text" name="nombre" id="inputNombre" class="form-control" value="" placeholder="Nombre" required="required">
					</div>
				</div>
				<div class="form-group">
					<label for="inputRazonSocial" class="col-sm-2 control-label">Razon Social:</label>
					<div class="col-sm-10">
						<input type="text" name="razonSocial" id="inputRazonSocial" class="form-control"  required="required" >
					</div>
				</div>
				<div class="form-group">
					<label for="inputRfc" class="col-sm-2 control-label">RFC:</label>
					<div class="col-sm-10">
						<input type="text" name="rfc" id="inputRfc" class="form-control" value="" required="required" placeholder="RFC">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="inputId" class="form-control">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@stop