@extends('app')
@section('Scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$("#btn-asignar").click(function(event) {
			$("#form-equipos").submit();
		});
	});
</script>
@stop
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
				<li>
					<a href="{{ url('/') }}">Principal</a> <span class="divider">/</span>
				</li>
				<li>
					<a href="{{ url('/empleados') }}">Empleados</a> <span class="divider">/</span>
				</li>
				<li>
					<a href="{{ url('/empleado/'.$empleado->id) }}">Empleados</a> <span class="divider">/</span>
				</li>
				<li class="active">
					Asignar nuevo equipo
				</li>
			</ul>
			<h3 class="text-primary">
				Asignar Empleados
			</h3>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
						Datos Empleado
					</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-6 text-primary">Nombre:</div>
								<div class="col-md-6">{{$empleado->nombre.' '.$empleado->ap_paterno.' '.$empleado->ap_materno}}</div>
							</div>
							<div class="row">
								<div class="col-md-6 text-primary">Puesto</div>
								<div class="col-md-6">{{$empleado->puesto}}</div>
							</div>
							<div class="row">
								<div class="col-md-6 text-primary">Servicio</div>
								<div class="col-md-6">{{$empleado->servicio->nombre}}</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-6 text-primary">Horario</div>
								<div class="col-md-6">{{$empleado->horarios}}</div>
							</div>
							<div class="row">
								<div class="col-md-6 text-primary">Telefono</div>
								<div class="col-md-6">{{$empleado->telefono}}</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			{!! Form::open(['url'=>'/equipos/asignar_equipo','method'=>'post','id'=>'form-equipos']) !!}
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Equipos Disponibles</h3>
				</div>
				<div class="panel-body">

					<table class="table table-hover table-condensed table-striped data-table">
						<thead>
							<tr>
								<th>
									Asignar
								</th>
								<th>Equipo</th>
								<th>Marca</th>
								<th>Modelo</th>
								<th>Serie</th>
								<th>Inventario</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($equipos as $row)
							<tr>
								<td>{!! Form::checkbox('equipos[]', $row->id, false, ['class'=>'form-control']) !!}</td>
								<td>{{$row->periferico->tipo}}</td>
								<td>{{$row->marca->marca}}</td>
								<td>{{$row->modelo}}</td>
								<td>{{$row->serie}}</td>
								<td>{{$row->inventario}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="panel-footer">
					<div class="row">
						<div class="col-md-12 btn-group pull-right">
							{!! Form::hidden('empleado', $empleado->id, ['id'=>'empleado']) !!}
							<button class="btn btn-success" type="submit" id="btn-asignar">Asignar Equipos</button>
						</div>
					</div>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Equipos Asignados</h3>
				</div>
				<div class="panel-body">
					<table class="table table-hover table-condensed table-striped data-table">
						<thead>
							<tr>
								<th>Equipo</th>
								<th>Marca</th>
								<th>Modelo</th>
								<th>Serie</th>
								<th>Inventario</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($equipos_asignados as $row)
							<?php $equipo=$row->equipos; ?>
							<tr>
								<td>{{$equipo->periferico->tipo}}</td>
								<td>{{$equipo->marca->marca}}</td>
								<td>{{$equipo->modelo}}</td>
								<td>{{$equipo->serie}}</td>
								<td>{{$equipo->inventario}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@stop