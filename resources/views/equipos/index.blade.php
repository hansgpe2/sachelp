@extends('app')
@section('content')
<div class="page-title">
	<div class="title_left">
		<h3>Inventario de Equipos</h3>
	</div>
	<div class="title_right">
		<a class="btn btn-round btn-success" data-toggle="modal" href='#modal-equipo'>Nuevo Equipo</a>
	</div>
</div>
<div class="clearfix"></div>
<div class="row clearfix">
	@if (Session::has('error'))
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Error</strong> {{ Session::get('error') }}
	</div>
	@endif
	@if (Session::has('mensaje'))
	<div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Movimiento realizado</strong> {{ Session::get('mensaje') }}
	</div>
	@endif
</div>
<div class="row clearfix">
	<table class="table table-striped table-hover" id="data-table">
		<thead>
			<tr>
				<th>Acciones</th>
				<th>Serie</th>
				<th>Numero de Inventario</th>
				<th>Marca</th>
				<th>Modelo</th>
				<th>Tipo de equipo</th>
				<th>Tipo de adquicision</th>
				<th>Otorgante</th>
				<th>Estatus</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($equipos as $row)
			<tr>
				<td><div class="btn-group">
					<a href="" class="btn btn-success">Modificar Datos</a>
					<a href="" class="btn btn-default">Eliminar</a>
				</div></td>
				<td>{{ $row->serie }}</td>
				<td>{{ $row->inventario }}</td>
				<td>{{ $row->marca->marca }}</td>
				<td>{{ $row->modelo }}</td>
				<td>{{ $row->periferico->tipo }}</td>
				<td>{{ $row->adquisicion->tipo }}</td>
				<td>{{ $row->otorgante->razonSocial }}</td>
				<td>
					@if ($row->activo==1)
						Activo
					@else
						Baja
					@endif
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<div class="modal fade" id="modal-equipo">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			{!! Form::open(['url'=>'/equipos/nuevo','method'=>'post','class'=>'form-horizontal']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Nuevo Equipo</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="inputTipo" class="col-sm-2 control-label">Tipo:</label>
					<div class="col-sm-4">
						{!! Form::select('tipo', $tipos, 0, ['id'=>'inputTipo','class'=>'form-control','required'=>'required']) !!}
					</div>
					<label for="inputMarca" class="col-sm-2 control-label">Marca:</label>
					<div class="col-sm-4">
						{!! Form::select('marca', $marcas, 0, ['id'=>'inputMarca','class'=>'form-control','required'=>'required']) !!}
					</div>
				</div>
				<div class="form-group">
					<label for="inputModelo" class="col-sm-2 control-label">Modelo:</label>
					<div class="col-sm-10">
						<input type="text" name="modelo" id="inputModelo" class="form-control"  required="required" >
					</div>
				</div>
				<div class="form-group">
					<label for="inputSerie" class="col-sm-2 control-label">Serie:</label>
					<div class="col-sm-4">
						<input type="text" name="serie" id="inputSerie" class="form-control" required="required">
					</div>
					<label for="inputInventario" class="col-sm-2 control-label">Numero Inventario:</label>
					<div class="col-sm-4">
						<input type="text" name="inventario" id="inputInventario" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label for="inputAdquicision" class="col-sm-2 control-label">Tipo de Adquicision:</label>
					<div class="col-sm-4">
						{!! Form::select('adquicision', $adquicision, 0, ['class'=>'form-control','id'=>'inputAdquicision']) !!}
					</div>
					<label for="inputProveedor" class="col-sm-2 control-label">Proveedor:</label>
					<div class="col-sm-4">
						{!! Form::select('proveedor', $proveedor, 0, ['class'=>'form-control','id'=>'inputProveedor']) !!}
					</div>
				</div>
				<div class="form-group">
					<label for="inputFecha_alta" class="col-sm-2 control-label">Fecha de alta:</label>
					<div class="col-sm-4">
						<input type="date" name="fecha_alta" id="inputFecha_alta" class="form-control" required="required">
					</div>
					<label for="inputCosto" class="col-sm-2 control-label">Costo:</label>
					<div class="col-sm-4">
						<input type="number" step="0.50" name="costo" id="inputCosto" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label for="inputDireccion_ip" class="col-sm-2 control-label">Direccion ip:</label>
					<div class="col-sm-10">
						<input type="text" name="direccion_ip" id="inputDireccion_ip" class="form-control"  pattern="^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$">
					</div>
				</div>
				<div class="form-group">
					<label for="textareaObservaciones" class="col-sm-2 control-label">Observaciones:</label>
					<div class="col-sm-10">
						<textarea name="observaciones" id="textareaObservaciones" class="form-control" rows="3"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Nuevo</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@stop