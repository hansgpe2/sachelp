@extends('app')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h3 class="text-primary">
				Datos del Equipo
			</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
						Datos generales
					</h3>
				</div>
				<div class="panel-body">
					<div class="row">
							<div class="col-md-8">
								<div class="row">
									<div class="col-md-6 text-primary">Marca</div>
									<div class="col-md-6">{{$equipo->marca->marca}}</div>
								</div>
								<div class="row">
									<div class="col-md-6 text-primary">Modelo</div>
									<div class="col-md-6">{{$equipo->modelo}}</div>
								</div>
								<div class="row">
									<div class="col-md-6 text-primary">Tipo</div>
									<div class="col-md-6">{{$equipo->periferico->tipo}}</div>
								</div>
								<div class="row">
									<div class="col-md-6 text-primary">Numero de serie</div>
									<div class="col-md-6">{{$equipo->serie}}</div>
								</div>
								<div class="row">
									<div class="col-md-6 text-primary">Numero de Inventario</div>
									<div class="col-md-6">{{$equipo->inventario}}</div>
								</div>
								<div class="row">
									<div class="col-md-6 text-primary">Tipo de Adquisición</div>
									<div class="col-md-6">{{$equipo->adquisicion->tipo}}</div>
								</div>
								<div class="row">
									<div class="col-md-6 text-primary">Proveedor</div>
									<div class="col-md-6">{{$equipo->otorgante->razonSocial}}</div>
								</div>
								<div class="row">
									<div class="col-md-6">Costo</div>
									<div class="col-md-6">{{$equipo->costo}}</div>
								</div>
								<div class="row">
									<div class="col-md-6">Dirección IP</div>
									<div class="col-md-6">{{$equipo->direccion_ip}}</div>
								</div>
								<div class="row">
									<div class="col-md-6">Observaciones</div>
									<div class="col-md-6">{{$equipo->observaciones}}</div>
								</div>
							</div>
							<div class="col-md-4 btn-group-vertical">
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-modificar-equipo"><span class="glyphicon glyphicon-pencil"></span> Modificar Datos</button>
								<a href="#modal-baja-equipo" data-toggle="modal" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Dar de Baja</a>
								<a href="{{ url('/equipos/codigoBarras/'.$equipo->id) }}" class="btn btn-default" target="_blank"><span class="glyphicon glyphicon-barcode"></span> Imprimir Etiqueta</a>
							</div>
						</div>	
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Ultimos Usuarios</h3>
			</div>
			<div class="panel-body">
				<table class="table table-condensed table-hover table-striped">
				<thead>
					<tr>
						<th>
							Nombre
						</th>
						<th>
							Puesto
						</th>
						<th>
							Servicio
						</th>
						<th>
							Fecha
						</th>
					</tr>
				</thead>
				<tbody>
				@foreach ($usuarios as $row)
				<?php $empleado=$row->empleado; ?>
					<tr>
						<td>
							{{$empleado->nombre.' '.$empleado->ap_paterno.' '.$empleado->ap_materno}}
						</td>
						<td>
							{{$empleado->puesto}}
						</td>
						<td>
							{{$empleado->servicio->nombre}}
						</td>
						<td>
							{{$row->fecha_asignacion}}
						</td>
					</tr>
				@endforeach	
				</tbody>
			</table>
			</div>
		</div>
			
		</div>
		<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Ultimos Reportes</h3>
			</div>
			<div class="panel-body">
				<table class="table table-condensed table-hover table-striped">
				<thead>
					<tr>
						<th>
							Folio
						</th>
						<th>
							Falla
						</th>
						<th>
							Responsable
						</th>
						<th>
							Estatus
						</th>
					</tr>
				</thead>
				<tbody>

					<tr>
						<td>
							1
						</td>
						<td>
							TB - Monthly
						</td>
						<td>
							01/04/2012
						</td>
						<td>
							Default
						</td>
					</tr>
					
				</tbody>
			</table>
			</div>
		</div>
			
		</div>
	</div>
</div>
<div class="modal fade" id="modal-modificar-equipo">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		{!! Form::open(['url'=>'equipos/update','class'=>'form-horizontal','method'=>'post']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Modificar Datos del Equipo</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="inputTipo" class="col-sm-2 control-label">Tipo:</label>
					<div class="col-sm-4">
						{!! Form::select('tipo', $tipos, $equipo->id_periferico, ['id'=>'inputTipo','class'=>'form-control','required'=>'required']) !!}
					</div>
					<label for="inputMarca" class="col-sm-2 control-label">Marca:</label>
					<div class="col-sm-4">
						{!! Form::select('marca', $marcas, $equipo->id_marca, ['id'=>'inputMarca','class'=>'form-control','required'=>'required']) !!}
					</div>
				</div>
				<div class="form-group">
					<label for="inputModelo" class="col-sm-2 control-label">Modelo:</label>
					<div class="col-sm-10">
						<input type="text" name="modelo" id="inputModelo" class="form-control"  required="required" value="{{$equipo->modelo}}">
					</div>
				</div>
				<div class="form-group">
					<label for="inputSerie" class="col-sm-2 control-label">Serie:</label>
					<div class="col-sm-4">
						<input type="text" name="serie" id="inputSerie" class="form-control" required="required" value="{{$equipo->serie}}">
					</div>
					<label for="inputInventario" class="col-sm-2 control-label">Numero Inventario:</label>
					<div class="col-sm-4">
						<input type="text" name="inventario" id="inputInventario" class="form-control" required="required" value="{{$equipo->inventario}}">
					</div>
				</div>
				<div class="form-group">
					<label for="inputAdquicision" class="col-sm-2 control-label">Tipo de Adquicision:</label>
					<div class="col-sm-4">
					{!! Form::select('adquicision', $adquicision, $equipo->id_adquisicion, ['class'=>'form-control','id'=>'inputAdquicision']) !!}
					</div>
					<label for="inputProveedor" class="col-sm-2 control-label">Proveedor:</label>
					<div class="col-sm-4">
					{!! Form::select('proveedor', $proveedor, $equipo->id_otorgante, ['class'=>'form-control','id'=>'inputProveedor']) !!}
					</div>
				</div>
				<div class="form-group">
					<label for="inputFecha_alta" class="col-sm-2 control-label">Fecha de alta:</label>
					<div class="col-sm-4">
						<input type="date" name="fecha_alta" id="inputFecha_alta" class="form-control" required="required" value="{{$equipo->fecha_alta}}">
					</div>
					<label for="inputCosto" class="col-sm-2 control-label">Costo:</label>
					<div class="col-sm-4">
						<input type="number" step="0.50" name="costo" id="inputCosto" class="form-control" value="{{$equipo->costo}}">
					</div>
				<label for="inputCosto" class="col-sm-2 control-label">Costo:</label>
					<div class="col-sm-4">
						<input type="number" step="0.50" name="costo" id="inputCosto" class="form-control" value="{{$equipo->costo}}">
					</div>
				</div>
				<div class="form-group">
					<label for="inputDireccion_ip" class="col-sm-2 control-label">Direccion ip:</label>
					<div class="col-sm-10">
						<input type="text" name="direccion_ip" id="inputDireccion_ip" class="form-control"  pattern="^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$" value="{{$equipo->direccion_ip}}">
					</div>
				</div>
				<div class="form-group">
					<label for="textareaObservaciones" class="col-sm-2 control-label">Observaciones:</label>
					<div class="col-sm-10">
						<textarea name="observaciones" id="textareaObservaciones" class="form-control" rows="3">{{$equipo->observaciones}}</textarea>
					</div>
				</div>
				<input type="hidden" name="idEquipo" value="{{$equipo->id}}">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
	</div>
	<div class="modal fade" id="modal-baja-equipo">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
			{!! Form::open(['url'=>'equipos/baja','method'=>'post','class'=>'form-horizontal']) !!}
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Baja de Equipo</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="textareaMotivo" class="col-sm-2 control-label">Motivo:</label>
						<div class="col-sm-10">
							<textarea name="motivo" id="textareaMotivo" class="form-control" rows="3" required="required"></textarea>
						</div>
					</div>
					<input type="hidden" name="id" value="{{$equipo->id}}">
					<input type="hidden" name="estatus" value="-1">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-primary">Dar de baja</button>
				</div>
				{!! Form::close() !!}
			</div>
		</div>	
</div>
@stop