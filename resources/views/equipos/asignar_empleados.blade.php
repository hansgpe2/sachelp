@extends('app')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li>
					<a href="{{ url('/') }}">Principal</a>
				</li>
				<li class="active">Asignar Equipos a Empleados</li>
			</ol>
		</div>
		<div class="page-title">
			<div class="title_left">
				<h3>Asignar Equipos a Empleado</h3>
			</div>
		</div>

		<div class="row clearfix">
			<div class="col-md-12">
				<table class="table table-striped" id="data-table">
					<thead>
						<tr>
							<th>Acciones</th>
							<th>Apellido Paterno</th>
							<th>Apellido Materno</th>
							<th>Nombre</th>
							<th>puesto</th>
							<th>Servicio</th>
							<th>Telefono</th>
							<th>Horarios</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($empleados as $row)
						<tr>
							<td class="btn-group">
								<a href="{{ url('/equipos/asignar_equipo/'.$row->id) }}" class="btn btn-default"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Asignar Equipos</a>
							</td>
							<td>
								{{$row->ap_paterno}}
							</td>
							<td>
								{{$row->ap_materno}}
							</td>
							<td>
								{{$row->nombre}}
							</td>
							<td>
								{{$row->puesto}}
							</td>
							<td>
								{{$row->servicio->nombre}}
							</td>
							<td>{{$row->telefono}}</td>
							<td>{{$row->horarios}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@stop