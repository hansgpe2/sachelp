@extends('app')
@section('Scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $("#btn-eliminar").click(function(event) {
        return confirm('Desea eliminar este empleado');
    });
  });
</script>
@endsection
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <ul class="breadcrumb">
        <li><a href="{{ url('/') }}">Principal</a></li>
        <li><a href="{{ url('empleados') }}">Empleados</a></li>
        <li class="active">Datos Empleado</li>
      </ul>

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Datos Empleado</h3>
        </div>

        <div class="panel-body">
          <div class="row">
            <div class="col-md-8">
              <div class="row">
                <div class="col-md-6 text-primary">Nombre:</div>
                <div class="col-md-6">{{$empleado->nombre.' '.$empleado->ap_paterno.' '.$empleado->ap_materno}}</div>
              </div>
              <div class="row">
                <div class="col-md-6 text-primary">Puesto</div>
                <div class="col-md-6">{{$empleado->puesto}}</div>
              </div>
              <div class="row">
                <div class="col-md-6 text-primary">Servicio</div>
                <div class="col-md-6">{{$empleado->servicio->nombre}}</div>
              </div>
              <div class="row">
                <div class="col-md-6 text-primary">Horario</div>
                <div class="col-md-6">{{$empleado->horarios}}</div>
              </div>
              <div class="row">
                <div class="col-md-6 text-primary">Telefono</div>
                <div class="col-md-6">{{$empleado->telefono}}</div>
              </div>
            </div>

            <div class="col-md-4 btn-group-vertical">
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-datos-generales"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Modificar</button>
                <a href="{{ url('/empleados/eliminar/'.$empleado->id) }}" class="btn btn-danger" id="btn-eliminar"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Eliminar Empleado</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <h3 class="text-primary">Equipos Asignados</h3>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <a class="btn btn-lg btn-success" href="{{ url('/equipos/asignar_equipo/'.$empleado->id) }}">Asignar Equipo</a>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <table class="table table-striped table-hover" id="data-table">
        <thead>
          <tr>
            <th>Acciones</th>
            <th>Equipo</th>
            <th>Marca</th>
            <th>Modelo</th>
            <th>Serie</th>
            <th>Inventario</th>
            <th>Fecha de Asignación</th>
          </tr>
        </thead>

        <tbody>
          @foreach ($equipos as $row)
          <?php $equipo=$row->equipo; ?>
          <tr>
            <td class="btn-group">
              <button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Retirar Equipo</button>
            </td>
            <td>{{$equipo->perifericos->tipo}}</td>
            <td>{{$equipo->marca->marca}}</td>
            <td>{{$equipo->modelo}}</td>
            <td>{{$equipo->serie}}</td>
            <td>{{$equipo->inventario}}</td>
            <td>{{date('d-m-Y',strtotime($row->fecha_asignacion))}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-datos-generales">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      {!! Form::open(['url'=>'/empleado/update','class'=>'form-horizontal','method'=>'post']) !!}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Datos Generales</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="inputPaterno" class="col-sm-2 control-label">Apellido Paterno:</label>
          <div class="col-sm-4">
          <input type="text" name="paterno" id="inputPaterno" class="form-control" required="required" value="{{$empleado->ap_paterno}}" >
          </div>          
          <label for="inputMaterno" class="col-sm-2 control-label">Apellido Materno:</label>
          <div class="col-sm-4">
            <input type="text" name="materno" id="inputMaterno" class="form-control"  required="required" value="{{$empleado->ap_materno}}">
          </div>
        </div>
        <div class="form-group">
          <label for="inputNombre" class="col-sm-2 control-label">Nombre:</label>
          <div class="col-sm-10">
            <input type="text" name="nombre" id="inputNombre" class="form-control"  required="required" value="{{$empleado->nombre}}">
          </div>
        </div>
        <div class="form-group">
          <label for="inputServicio" class="col-sm-2 control-label">Servicio:</label>
          <div class="col-sm-4">
            {!! Form::select('servicio', $servicios, $empleado->id_servicio, ['class'=>'form-control','id'=>'inputServicio']) !!}
          </div>
          <label for="inputHorarios" class="col-sm-2 control-label">Horarios:</label>
          <div class="col-sm-4">
            <input type="text" name="horarios" id="inputHorarios" class="form-control" required="required" value="{{$empleado->horarios}}">
          </div>          
        </div>
        <div class="form-group">
          <label for="inputTelefono" class="col-sm-2 control-label">Telefono:</label>
          <div class="col-sm-4">
            <input type="text" name="telefono" id="inputTelefono" class="form-control" required="required" value="{{$empleado->telefono}}">
          </div>
          <label for="inputPuesto" class="col-sm-2 control-label">Puesto:</label>
          <div class="col-sm-4">
            <input type="text" name="puesto" id="inputPuesto" class="form-control" required="required" value="{{$empleado->puesto}}">
          </div>
        </div>
      </div>
      <div class="modal-footer">
      <input type="hidden" name="id" value="{{$empleado->id}}">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection