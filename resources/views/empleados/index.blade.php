@extends('app')
@section('content')
<div class="container-fluid">
	<div class="page-title">
		<div class="title_left">
			<h3>Catalogo de Empleados</h3>
		</div>
		<div class="title_right">
			<a class="btn btn-round btn-primary" data-toggle="modal" href='#modal-datos-generales'><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Nuevo Empleado</a>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row clearfix">
		@if (Session::has('error'))
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Error</strong> {{ Session::get('error') }}
		</div>
		@endif
		@if (Session::has('mensaje'))
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Movimiento realizado</strong> {{ Session::get('mensaje') }}
		</div>
		@endif
	</div>
	<div class="row clearfix">
		<div class="col-md-12">
			<table class="table table-striped" id="data-table">
				<thead>
					<tr>
						<th>Acciones</th>
						<th>Apellido Paterno</th>
						<th>Apellido Materno</th>
						<th>Nombre</th>
						<th>puesto</th>
						<th>Servicio</th>
						<th>Telefono</th>
						<th>Horarios</th>
					</tr>
				</thead>
				<tbody>
				@foreach ($empleados as $row)
					<tr>
						<td class="btn-group">
							<a href="{{ url('/empleado/'.$row->id) }}" class="btn btn-default"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Modificar</a>
							<button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Eliminar</button>
						</td>
						<td>
							{{$row->ap_paterno}}
						</td>
						<td>
							{{$row->ap_materno}}
						</td>
						<td>
							{{$row->nombre}}
						</td>
						<td>
							{{$row->puesto}}
						</td>
						<td>
							{{$row->servicio->nombre}}
						</td>
						<td>{{$row->telefono}}</td>
						<td>{{$row->horarios}}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-datos-generales">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		{!! Form::open(['url'=>'/empleados/nuevo','class'=>'form-horizontal','method'=>'post']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Datos Generales</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="inputPaterno" class="col-sm-2 control-label">Apellido Paterno:</label>
					<div class="col-sm-4">
						<input type="text" name="paterno" id="inputPaterno" class="form-control" required="required"  >
					</div>					
					<label for="inputMaterno" class="col-sm-2 control-label">Apellido Materno:</label>
					<div class="col-sm-4">
						<input type="text" name="materno" id="inputMaterno" class="form-control"  required="required">
					</div>
				</div>
				<div class="form-group">
					<label for="inputNombre" class="col-sm-2 control-label">Nombre:</label>
					<div class="col-sm-10">
						<input type="text" name="nombre" id="inputNombre" class="form-control"  required="required">
					</div>
				</div>
				<div class="form-group">
					<label for="inputServicio" class="col-sm-2 control-label">Servicio:</label>
					<div class="col-sm-4">
					{!! Form::select('servicio', $servicios, 0, ['class'=>'form-control','id'=>'inputServicio']) !!}
					</div>
						<label for="inputHorarios" class="col-sm-2 control-label">Horarios:</label>
						<div class="col-sm-4">
							<input type="text" name="horarios" id="inputHorarios" class="form-control" required="required">
						</div>					
				</div>
				<div class="form-group">
					<label for="inputTelefono" class="col-sm-2 control-label">Telefono:</label>
					<div class="col-sm-4">
						<input type="text" name="telefono" id="inputTelefono" class="form-control" required="required">
					</div>
					<label for="inputPuesto" class="col-sm-2 control-label">Puesto:</label>
					<div class="col-sm-4">
						<input type="text" name="puesto" id="inputPuesto" class="form-control" required="required">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@stop