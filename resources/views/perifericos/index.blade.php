@extends('app')
@section('Scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			$("#modal-modificar-marcas").on('show.bs.modal', function(event) {
				id=$(event.relatedTarget).data('idmarca');
				$.getJSON('{{ url('perifericos/') }}/'+id, function(json, textStatus) {
						$("#idmarca").val(json['id']);
						$("#marcaModificar").val(json['marca']);
				});
			});
		});
	</script>
@endsection
@section('content')
	<div class="page-title">
		<div class="title_left">
			<h3>Catalogo de Perifericos</h3>
		</div>
		<div class="title_right">
			<a class="btn btn-round btn-success" data-toggle="modal" href='#modal-marcas' data-idmarca="0">Nuevo periferico</a>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row clearfix">
	@if (Session::has('error'))
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Error</strong> {{ Session::get('error') }}
		</div>
	@endif
	@if (Session::has('mensaje'))
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Movimiento realizado</strong> {{ Session::get('mensaje') }}
		</div>
	@endif
	</div>
	<div class="row clearfix">
		<div class="col-md-12 col-xs-12">
			<table class="table table-striped table-condensed table-hover" id="data-table">
				<thead>
					<th>Acciones</th>
					<th>Marca</th>
				</thead>
				<tbody>
				@foreach ($perifericos as $row)
					<tr>
						<td>
							<div class="btn-group">
								<a class="btn btn-round btn-primary" data-toggle="modal" href='#modal-modificar-marcas' data-idmarca="{{ $row->id }}">Modificar</a>
								<a href="{{ url('/perifericos/destroy/'.$row->id) }}" class="btn btn-round btn-danger">Eliminar</a>
							</div>
						</td>
						<td>{{ $row->tipo }}</td>
					</tr>
				@endforeach
					
				
				</tbody>
			</table>
		</div>
	</div>

	<div class="modal fade" id="modal-marcas">
		<div class="modal-dialog">
			<div class="modal-content">
			{!! Form::open(['url'=>'/nuevo_periferico','method'=>'post','data-parsley-validate','class'=>'form-horizontal']) !!}
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Nuevo Periferico</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="inputPeriferico" class="col-sm-2 control-label">Periferico:</label>
						<div class="col-sm-10">
							<input type="text" name="periferico" id="inputPeriferico" class="form-control" required="required">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-primary">Agregar</button>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-modificar-marcas">
		<div class="modal-dialog">
			<div class="modal-content">
			{!! Form::open(['url'=>'/perifericos_update','method'=>'post','class'=>'form-horizontal','data-parsley-validate']) !!}
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Modificar Periferico</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="perifericoModificar" class="col-sm-2 control-label">Periferico:</label>
						<div class="col-sm-10">
							<input type="text" name="periferico" id="perifericoModificar" class="form-control" required="required">
							<input type="hidden" name="id" id="idmarca">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-primary">Modificar</button>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop