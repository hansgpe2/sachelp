@extends('app')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			<img src="{{ asset('images/logoEncabezado2016.jpg') }}" class="img-responsive center-block" alt="Image">
			<br>
			<h1>{{Config::get('app.titulo')}}
			<br><small>{{Config::get('app.dependencia')}} {{Config::get('app.unidad')}}</small></h1>
			<hr>
			Uso exclusivo para el departamento de {{Config::get('app.departamento')}}

		</div>
		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Error</strong> Verifique los datos de acceso<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
			<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="form-group">
							<label class="col-md-4 control-label">Correo électronico</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="remember"> Recordar password
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">Entrar</button>
								</div>
						</div>
					</form>
		</div>
	</div>
</div>
@stop
