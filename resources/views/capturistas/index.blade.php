@extends('app')
@section('Scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			$("#modal-capturista").on('show.bs.modal', function(event) {
				id=$(event.relatedTarget).data('idcapturista');
				if(id==0)
				{
					$("#form-capturista").attr('action', '{{ url('capturistas/nuevo') }}');
					$("#inputNombre").val('');
					$("#inputId").val('');
				}
				else {
					$("#form-capturista").attr('action', '{{ url('capturistas/modificar') }}');
					$.getJSON('capturista/'+id, function(json, textStatus) {
							$("#inputNombre").val(json['nombre']);
							$("#inputId").val(json['id']);
					});
				}
			});
		});
	</script>
@endsection
@section('content')
	<div class="page-title">
	<div class="title_left">
		<h3>Catalogo de Capturistas</h3>
	</div>
	<div class="title_right">
		<a class="btn btn-round btn-primary" data-toggle="modal" href='#modal-capturista' data-idcapturista="0">Nuevo Capturista</a>
	</div>
</div>
<div class="clearfix"></div>
<div class="row clearfix">
	@if (Session::has('error'))
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Error</strong> {{ Session::get('error') }}
	</div>
	@endif
	@if (Session::has('mensaje'))
	<div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Movimiento realizado</strong> {{ Session::get('mensaje') }}
	</div>
	@endif
</div>
<div class="clearfix"></div>
<div class="row clearfix">
	<table class="table table-striped table-hover" id="data-table">
		<thead>
			<tr>
				<th>Acciones</th>
				<th>Nombre</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($capturistas as $row)
			<tr>
				<td><div class="btn-group">
					<a href="#modal-capturista" data-toggle="modal" data-idcapturista="{{ $row->id }}" class="btn btn-round btn-default"><i class="fa fa-edit"></i> Editar</a>
					<a href="{{ url('capturistas/eliminar/'.$row->id) }}" class="btn btn-round btn-danger"><i class="fa fa-erase"></i> Eliminar</a>
				</div></td>
				<td>
					{{ $row->nombre }}
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>
<div class="modal fade" id="modal-capturista">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['url'=>'','method'=>'post','class'=>'form-horizontal','id'=>'form-capturista']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Datos de Capturista</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="inputNombre" class="col-sm-2 control-label">Nombre:</label>
					<div class="col-sm-10">
						<input type="text" name="nombre" id="inputNombre" class="form-control"  required="required">
						<input type="hidden" name="id" id="inputId" class="form-control">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@stop