<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<title>
		{{Config::get('app.titulo')}} - {{Config::get('app.dependencia').' '.Config::get('app.unidad')}}
	</title>
	<link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/bootstrap-theme-issste.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/datatables.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/dataTables.bootstrap.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/select2.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/select2-bootstrap.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/timepicker.css') }}" rel="stylesheet">
	<link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
	<!-- Fonts -->
	<link href="//fonts.googleapis.com/css?family=Roboto:400,300" rel="stylesheet" type="text/css">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                                                                <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<!-- Datatables -->
		<link href="{{ asset('datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">

		<!-- Scripts -->
		<script src="{{ asset('js/jquery-2.2.0.min.js') }}">
		</script>
		<script src="{{ asset('js/bootstrap.min.js') }}">
		</script>
		

		<!-- Datatables -->
		<script src="{{ asset('datatables.net/js/jquery.dataTables.min.js') }}"></script>
		<script src="{{ asset('datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
		<script src="{{ asset('datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
		<script src="{{ asset('datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
		<script src="{{ asset('datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
		<script src="{{ asset('datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
		<script src="{{ asset('datatables.net-buttons/js/buttons.print.min.js') }}"></script>
		<script src="{{ asset('datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
		<script src="{{ asset('datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
		<script src="{{ asset('datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
		<script src="{{ asset('datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
		<script src="{{ asset('datatables.net-scroller/js/datatables.scroller.min.js') }}"></script>
		

		<!-- Custom Theme Scripts -->
		<script src="{{ asset('js/custom.js') }}"></script>

		@yield('Scripts')
	</head>
	<body style="padding-top: 100px;">
		<div class="container-fluid">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
				<div class="container">
					<a class="navbar-brand" href="{{ url('/') }}">{{Config::get('app.titulo')}}</a>
					@if (!Auth::guest())
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">


							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" role="button"> Incidencias <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="{{ url('/incidencias/nuevo') }}">Levantar incidencia</a></li>
									<li><a href="{{ url('/incidencias/') }}">Seguimiento de incidencias</a></li>
								</ul>
							</li>
							<li>
							<a class="dropdown-toggle" data-toggle="dropdown" role="button"> Equipos <span class="caret"></span></a>
								<ul class="dropdown-menu multi-level">
									<li><a href="{{ url('/equipos') }}">Control de equipos</a></li>
									<li class="dropdown-submenu">
										<a class="dropdown-toggle" data-toggle="dropdown" role="button">Asignación a usuarios <span class="caret"></span></a>
										<ul class="dropdown-menu">
											<li><a href="{{ url('/equipos/asignar_empleados/') }}" title="Asignacion de equipo por empleado">Asignacion por empleado</a></li>
											<li><a href="" title="Asignacion por Equipo">Asignacion por Equipo</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li><a class="dropdown-toggle" data-toggle="dropdown" role="button"> Catalogos <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="{{ url('marcas') }}">Marcas de equipos</a></li>
									<li><a href="{{ url('perifericos') }}">Perifericos</a></li>
									<li><a href="{{ url('otorgantes') }}">Proveedores</a></li>
									<li><a href="{{ url('/servicios') }}">Servicios</a></li>
									{{--<li><a href="{{ url('/capturistas') }}">Capturistas</a></li> --}}
									<li><a href="{{ url('/users') }}">Usuarios</a></li>
									<li><a href="{{ url('/empleados') }}">Empleados</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" role="button"> Reportes <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="">Capturistas</a></li>
									<li><a href="">Tecnicos</a></li>
									<li><a href="">Equipos</a></li>
									<li><a href="">Incidencias</a></li>
								</ul>
							</li>
							<li>
								<a href="{{ url('/auth/logout') }}">
									Salir de la sesión
								</a>
							</li>
						</ul>
					@endif
					</div>
				</nav>
			</div>
			<div class="container-fluid">
				@yield('content')
			</div>

		</body>
		</html>
