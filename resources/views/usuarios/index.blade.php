@extends('app')
@section('content')
<div class="container-fluid">
	<div class="page-header">
		<div class="title_left">
			<h3>Control de Usuarios</h3>
		</div>
		<div class="title_right">
		<a class="btn btn-round btn-primary" href='{{ url('usuarios/nuevo') }}'><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo Usuario</a>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row clearfix">
		@if (Session::has('error'))
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Error</strong> {{ Session::get('error') }}
		</div>
		@endif
		@if (Session::has('mensaje'))
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Movimiento realizado</strong> {{ Session::get('mensaje') }}
		</div>
		@endif
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped" id="data-table">
				<thead>
					<tr>
						<th>Acciones</th>
						<th>login</th>
						<th>Perfil</th>
						<th>Estatus</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($usuarios as $row)

					<tr>
						<td class="btn-group">
							<a href="{{ url('users/'.$row->id) }}" class="btn btn-default"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Editar</a>
							@if($row->activo==1)
							<button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Desactivar</button>
							@else
							<button type="button" class="btn btn-success"><span class="glyphicon glyphicon-play" aria-hidden="true"></span> Activar</button>
							@endif
						</td>
						<td>{{$row->email}}</td>
						<td>{{$row->perfiles->perfiles->perfil}}</td>
						<td>
							@if ($row->activo==1)
							Activo
							@else
							Desactivado
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@stop