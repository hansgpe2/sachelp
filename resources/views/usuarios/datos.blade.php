@extends('app')
@section('Scripts')
	<script>
		$(document).ready(function() {
			$("#modal-activar").on('show.bs.modal', function(event) {
				activo=$(event.relatedTarget).data('accion');

				if(activo==1)
				{
					$("#mensajeActivacion").html('<h3>Desea Activar el usuario</h3>');
				}
				else
				{
					$("#mensajeActivacion").html('<h3>Desea Desactivar el usuario</h3>');	
				}
				$("#activo").val(activo);
			});
			$("#btn-modificar").click(function(event) {
				$("#form-datos").find('input').removeAttr('readonly');
				$("#form-datos").find('select').removeAttr('disabled');
				$("#btn-guardar-datos").removeAttr('disabled');
				$(this).attr('disabled', 'disabled');
			});
		});
	</script>
	<style type="text/css">
		.calidad
		{
			font-size: 42px;
			text-align: center;
			font-weight: bold;
		}
	</style>
@endsection
@section('content')
<div class="container-fluid">
	<div class="page-header">
	  <h3>Usuario<small>{{$usuario->name}}</small></h3>
	</div>
	<div class="row">
		<div class="col-md-6">
		{!! Form::open(['class'=>'form-horizontal']) !!}
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Datos de acceso</h3>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label for="inputLogin" class="col-sm-2 control-label">Login:</label>
						<div class="col-sm-10">
							<input type="text" name="login" id="inputLogin" class="form-control" value="{{$usuario->email}}" required="required" readonly="readonly">
						</div>
					</div>
					<div class="form-group btn-group center-block">
						<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-login">Modificar Acceso</button>
						@if($usuario->activo==1)
							<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-activar" data-accion="0">Desactivar</button>
						@else
							<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-activar" data-accion="1">Activar</button>
						@endif
					</div>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Datos Personales</h3>
				</div>
				<div class="panel-body">
					{!! Form::open(['url'=>'users/cambiarDatos','method'=>'post','class'=>'form-horizontal','id'=>'form-datos']) !!}
					<div class="form-group">
						<label for="inputNombre" class="col-sm-2 control-label">Nombre:</label>
						<div class="col-sm-10">
							<input type="text" name="nombre" id="inputNombre" class="form-control" value="{{$usuario->name}}" required="required" readonly="readonly">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPerfil" class="col-sm-2 control-label">Perfil:</label>
						<div class="col-sm-10">
						{!! Form::select('perfil', $perfiles, $usuario->perfiles->id_perfil, ['class'=>'form-control','id'=>'inputPerfil','required'=>'required', 'disabled'=>'disabled']) !!}
						<input type="hidden" name="id" value="{{$usuario->id}}">
						</div>
					</div>
					<div class="form-group center-block">
						<button type="button" class="btn btn-default" id="btn-modificar">Modificar Datos</button>
						<button type="submit" class="btn btn-success" id="btn-guardar-datos" disabled="disabled"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Guardar</button>
						
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Reportes</h3>
				</div>
				<div class="panel-body">
					<table class="table table-striped table-hover" id="data-table">
						<thead>
							<tr>
								<th>Ver Reporte</th>
								<th>Persona que Reporta</th>
								<th>Reporte</th>
								<th>Estatus</th>
								<th>Calidad</th>
							</tr>
						</thead>
						<tbody>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-danger">
				<div class="panel-heading">
					<h3 class="panel-title">Calidad en Reportes</h3>
				</div>
				<div class="panel-body calidad">
					0
				</div>
			</div>
		</div>
	</div>
</div>
{{-- Activacion de usuarios --}}
<div class="modal fade" id="modal-activar">
	<div class="modal-dialog">
		<div class="modal-content">
		{!!Form::open(['url'=>'users/delete','method'=>'post'])!!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Activar Usuario</h4>
			</div>
			<div class="modal-body">
				<div id="mensajeActivacion">
					<h4>Desea desactivar el usuario</h4>
				</div>
				
				<input type="hidden" name="idUsuario" value="{{$usuario->id}}">
				<input type="hidden" name="activo" value="" id="activo">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
			{!!Form::close()!!}
		</div>
	</div>
</div>
{{-- Cambiar Acceso --}}
<div class="modal fade" id="modal-login">
	<div class="modal-dialog">
		<div class="modal-content">
		{!!Form::open(['url'=>'users/cambiarAcceso','method'=>'post','class'=>'form-horizontal']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Cambiar Datos de Acceso</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="inputEmail" class="col-sm-2 control-label">Email:</label>
					<div class="col-sm-10">
						<input type="email" name="email" id="inputEmail" class="form-control" value="{{$usuario->email}}" required="required">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword" class="col-sm-2 control-label">Password:</label>
					<div class="col-sm-10">
						<input type="password" name="password" id="inputPassword" class="form-control" required="required">
					</div>
					<input type="hidden" name="idUsuario" value="{{$usuario->id}}">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-primary">Actualizar</button>
			</div>
			{!!Form::close()!!}
		</div>
	</div>
</div>

@stop