@extends('app')
@section('content')
<div class="container-fluid">
	<div class="page-header">
		<div class="title_left">
			<h3>Nuevo Usuario</h3>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row clearfix">
		@if (Session::has('error'))
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Error</strong> {{ Session::get('error') }}
		</div>
		@endif
		@if (Session::has('mensaje'))
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Movimiento realizado</strong> {{ Session::get('mensaje') }}
		</div>
		@endif
	</div>
	<div class="row">
	{!! Form::open(['url'=>'users/crear','role'=>'form','method'=>'post','class'=>'form-horizontal']) !!}
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Datos de Acceso</h3>
				</div>
				<div class="panel-body">
				
					<div class="form-group">
						<label for="inputEmail" class="col-sm-2 control-label">Email:</label>
						<div class="col-sm-10">
							<input type="email" name="email" id="inputEmail" class="form-control" value="" required="required">
						</div>
					</div>
					<div class="form-group">
							<label class="col-md-4 control-label">Contraseña</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirmar contraseña</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>
							
					
							
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Panel title</h3>
				</div>
				<div class="panel-body">
					<div class="form-group">
					<label for="inputNombre" class="col-sm-2 control-label">Nombre:</label>
					<div class="col-sm-10">
						<input type="text" name="name" id="inputNombre" class="form-control" value="" required="required">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPerfil" class="col-sm-2 control-label">Perfil:</label>
					<div class="col-sm-10">
					{!! Form::select('perfil',$perfiles,old('perfil'),['id'=>'inputPerfil','class'=>'form-control','required'=>'required']) !!}
					</div>
				</div>
				</div>
			</div>
		</div>
		<div class="form-group">
								<div class="col-sm-10 col-sm-offset-2">
									<button type="submit" class="btn btn-primary">Crear Usuario</button>
								</div>
							</div>
		{!! Form::close() !!}
	</div>
</div>
@stop