@extends('app')
@section('Scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			$("#modal-servicio").on('show.bs.modal', function(event) {
				id=$(event.relatedTarget).data('idservicio');
				if(id==0)
				{
					$("#form-servicio").attr('action', '{{ url('servicios/nuevo') }}');
					$("#inputId").val('');
					$("#inputservicio").val('');
				}
				else
				{
					$("#form-servicio").attr('action', '{{ url('servicios/modificar') }}');
					$.getJSON('{{ url('servicio') }}/'+id, function(json, textStatus) {
							$("#inputId").val(json['id']);
							$("#inputservicio").val(json['nombre']);
					});
				}
			});
		});
	</script>
@endsection
@section('content')
	<div class="page-title">
	<div class="title_left">
		<h3>Catalogo de Servicios</h3>
	</div>
	<div class="title_right">
		<a class="btn btn-round btn-primary" data-toggle="modal" href='#modal-servicio' data-idservicio="0">Nuevo Servicio</a>
	</div>
</div>
<div class="clearfix"></div>
<div class="row clearfix">
	@if (Session::has('error'))
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Error</strong> {{ Session::get('error') }}
	</div>
	@endif
	@if (Session::has('mensaje'))
	<div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Movimiento realizado</strong> {{ Session::get('mensaje') }}
	</div>
	@endif
</div>
<div class="clearfix"></div>
<div class="row clearfix">
	<table class="table table-striped table-header" id="data-table">
		<thead>
			<tr>
				<th>Acciones</th>
				<th>Nombre</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($servicios as $row)
			<tr>
				<td><div class="btn-group">
					<a href="#modal-servicio" data-toggle="modal" data-idservicio="{{ $row->id }}" class="btn btn-round btn-default"><i class="fa fa-edit"></i> Editar</a>
					<a href="{{ url('servicios/eliminar/'.$row->id) }}" class="btn btn-round btn-danger"><i class="fa fa-erase"></i> Eliminar</a>
				</div></td>
				<td>
					{{ $row->nombre }}
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>
<div class="modal fade" id="modal-servicio">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['url'=>'','method'=>'post','class'=>'form-horizontal','id'=>'form-servicio']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Datos de Servicio</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="inputservicio" class="col-sm-2 control-label">Servicio:</label>
					<div class="col-sm-10">
						<input type="text" name="servicio" id="inputservicio" class="form-control" required="required">
						<input type="hidden" name="id" id="inputId" class="form-control" value="">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@stop