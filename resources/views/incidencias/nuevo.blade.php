@extends('app')
@section('Scripts')
<script type="text/javascript">
	$(document).ready(function() {
		// $("#btn-buscar").click(function(event) {
		// 	app=$("#inputAp_paterno").val();
		// 	apm=$("#inputAp_materno").val();
		// 	nombre=$("#inputNombres").val();
		// 	datos='nombre='+nombre+'&ap_paterno='+app+'&ap_materno='+apm;
		// 	$.getJSON('{{ url('/empleados/listado') }}?'+datos, 
		// 		function(json, textStatus) {
		// 			$("#inputEmpleado").empty();
		// 			$.each(json, function(index,empleado) {
		// 				$("#inputEmpleado").append('<option value="'+empleado['id']+'">'+empleado['nombre']+' '+empleado['ap_paterno']+' '+empleado['ap_materno']+'</option>');
		// 			});
		// 		});
		// 	$("#btn-seleccionar").click(function(event) {
		// 		idEmpleado=$("#inputEmpleado").val();
		// 		$("#inputIdEmpleado").val(idEmpleado);
		// 		$.getJSON('{{ url('/ajax/empleado') }}/'+idEmpleado, function(json, textStatus) {
		// 			$("#inputPaterno").val(json['ap_paterno']);
		// 			$("#inputMaterno").val(json['ap_materno']);
		// 			$("#inputNombre").val(json['nombre']);
		// 			$("#inputTelefono").val(json['telefono']);
		// 			$("#inputServicio").val(json['servicio']['nombre']);
		// 		});
		// 	});
		// });

		$("#inputBuscar").select2({
			tags:true,
			tokenSeparators: [','],
			ajax: {
				url: "{{ url('/empleados/listado') }}",
				dataType: 'json',
				delay: 250,
				data: function(params) {
					return {
						term: params.term
					}
				},
				processResults: function (data, page) {
					return {
						results: data
					};
				},
			},
			minimumInputLength: 4
		});

		$("#inputBuscar").change(function(event) {
			idEmpleado=$(this).val();
				$("#inputIdEmpleado").val(idEmpleado);
				$.getJSON('{{ url('/ajax/empleado') }}/'+idEmpleado, function(json, textStatus) {
					$("#inputPaterno").val(json['ap_paterno']);
					$("#inputMaterno").val(json['ap_materno']);
					$("#inputNombre").val(json['nombre']);
					$("#inputTelefono").val(json['telefono']);
					$("#inputServicio").val(json['servicio']['nombre']);
				});
		});

		
	});
</script>
@endsection
@section('content')
<div class="container-fluid">
	<ol class="breadcrumb">
		<li>
			<a href="{{ url('/') }}">Principal</a>
		</li>
		<li class="active">Nuevo Reporte</li>
	</ol>
	<div class="clearfix"></div>
	<div class="row clearfix">
		@if (Session::has('error'))
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Error</strong> {{ Session::get('error') }}
		</div>
		@endif
		@if (Session::has('mensaje'))
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Movimiento realizado</strong> {{ Session::get('mensaje') }}
		</div>
		@endif
	</div>
	<div class="row">
		<div class="col-md-12">
			<h3 class="text-primary">Nuevo Reporte</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			{!! Form::open(['url'=>'incidencias/nuevo','class'=>'form-horizontal','method'=>'post']) !!}
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Datos del Reporte</h3>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label for="inputBuscar" class="col-sm-2 control-label">Buscar Empleado:</label>
						<div class="col-sm-8">
							<select name="buscar" id="inputBuscar" class="form-control">
								<option value=""></option>
							</select>
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button id="btn-buscar-empleado" class="btn btn-primary" type="button"><span class="glyphicon glyphicon-search"></span> Buscar Empleado</button>
						</div>
					</div>
					<div class="form-group">
						<label for="inputPaterno" class="col-sm-2 control-label">Paterno:</label>
						<div class="col-sm-4">
							<input type="text" name="paterno" id="inputPaterno" class="form-control" readonly="readonly">
						</div>
					</div>
					<div class="form-group">
						<label for="inputMaterno" class="col-sm-2 control-label">Materno:</label>
						<div class="col-sm-4">
							<input type="text" name="materno" id="inputMaterno" class="form-control" readonly="readonly">
							<input type="hidden" name="idEmpleado" id="inputIdEmpleado" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="inputNombre" class="col-sm-2 control-label">Nombre:</label>
						<div class="col-sm-6">
							<input type="text" name="nombre" id="inputNombre" class="form-control" readonly="readonly">
						</div>
						<div class="col-md-4">
						</div>
					</div>
					<div class="form-group">
						<label for="inputTelefono" class="col-sm-2 control-label">Telefono:</label>
						<div class="col-sm-4">
							<input type="text" name="telefono" id="inputTelefono" class="form-control" required="required">
						</div>
						<label for="inputServicio" class="col-sm-2 control-label">Servicio:</label>
						<div class="col-sm-4">
							<input type="text" name="servicio" id="inputServicio" class="form-control" required="required">
						</div>
					</div>
					<div class="form-group">
						<label for="textareaReporte" class="col-sm-2 control-label">Reporte:</label>
						<div class="col-sm-10">
							<textarea name="reporte" id="textareaReporte" class="form-control" rows="3" required="required"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="textareaObservaciones" class="col-sm-2 control-label">Observaciones:</label>
						<div class="col-sm-10">
							<textarea name="observaciones" id="textareaObservaciones" class="form-control" rows="3"></textarea>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<div class="btn-group">
						<a href="" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Regresar</a>
						<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Generar Reporte</button>
					</div>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="modal-buscar-empleado">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			{!! Form::open(['class'=>'form-horizontal']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Buscar Empleados</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="inputAp_paterno" class="col-sm-2 control-label">Apellido Paterno:</label>
					<div class="col-sm-4">
						<input type="text" name="ap_paterno" id="inputAp_paterno" class="form-control">
					</div>
					<label for="inputAp_materno" class="col-sm-2 control-label">Apellido Materno:</label>
					<div class="col-sm-4">
						<input type="text" name="ap_materno" id="inputAp_materno" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label for="inputNombres" class="col-sm-2 control-label">Nombres:</label>
					<div class="col-sm-6">
						<input type="text" name="nombres" id="inputNombres" class="form-control">
					</div>
					<div class="col-md-4">
						<button type="button" id="btn-buscar" class="btn btn-success"><span class="glyphicon glyphicon-search"></span> Buscar</button>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmpleado" class="col-sm-2 control-label">Empleado:</label>
					<div class="col-sm-10">
						<select name="empleado" id="inputEmpleado" class="form-control">
							<option value=""></option>
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-primary" id="btn-seleccionar" data-dismiss="modal">Seleccionar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@stop