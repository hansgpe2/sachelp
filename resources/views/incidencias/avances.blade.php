@extends('app')
@section('Scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$("#inputTecnico").blur(function(event) {
			tecnico=$(this).val();
			$.get('{{ url('/incidencias/asignar_tecnico/'.$reporte->id) }}/'+tecnico, function(data) {
				alert('Tecnico Asignado');
			});
		});
	});
</script>
@stop
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb">
				<li>
					<a href="{{ url('/') }}">Principal</a> <span class="divider">/</span>
				</li>
				<li>
					<a href="{{ url('/incidencias') }}">Incidencias</a> <span class="divider">/</span>
				</li>
				<li class="active">
					Reporte folio {{$reporte->id}}
				</li>
			</ul>
			<div class="row clearfix">
				@if (Session::has('error'))
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Error</strong> {{ Session::get('error') }}
				</div>
				@endif
				@if (Session::has('mensaje'))
				<div class="alert alert-info">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Movimiento realizado</strong> {{ Session::get('mensaje') }}
				</div>
				@endif
			</div>
			<h3>
				Reporte {{$reporte->id}}
			</h3>
			<div class="tabbable" id="tabs-373838">
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#panel-datos" data-toggle="tab">Datos del Reporte</a>
					</li>
					<li>
						<a href="#panel-seguimiento" data-toggle="tab">Avances del Reporte</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="panel-datos">
						<div class="row">
							<div class="col-md-12">
								{!! Form::open(['url'=>'incidencias/nuevo','class'=>'form-horizontal','method'=>'post']) !!}
								<div class="panel panel-primary">
									<div class="panel-heading">
										<h3 class="panel-title">Datos del Reporte</h3>
									</div>
									<div class="panel-body">
										<div class="form-group">
											<label for="inputEstatus" class="col-sm-2 control-label">Estatus:</label>
											<div class="col-sm-10">
												<input type="text" name="estatus" id="inputEstatus" class="form-control" value="{{$reporte->estado->descripcion}}" readonly="readonly">
											</div>
										</div>
										<div class="form-group">
											<label for="inputPaterno" class="col-sm-2 control-label">Paterno:</label>
											<div class="col-sm-4">
												<input type="text" name="paterno" id="inputPaterno" class="form-control" readonly="readonly" value="{{$reporte->empleado->ap_paterno}}">
											</div>
											<label for="inputMaterno" class="col-sm-2 control-label">Materno:</label>
											<div class="col-sm-4">
												<input type="text" name="materno" id="inputMaterno" class="form-control" readonly="readonly">
												<input type="hidden" name="idEmpleado" id="inputIdEmpleado" class="form-control" value="{{$reporte->empleado->ap_materno}}">
											</div>
										</div>
										<div class="form-group">
											<label for="inputNombre" class="col-sm-2 control-label">Nombre:</label>
											<div class="col-sm-4">
												<input type="text" name="nombre" id="inputNombre" class="form-control" readonly="readonly" value="{{$reporte->empleado->nombre}}">
											</div>
										</div>
										<div class="form-group">
											<label for="inputTelefono" class="col-sm-2 control-label">Telefono:</label>
											<div class="col-sm-4">
												<input type="text" name="telefono" id="inputTelefono" class="form-control" readonly="readonly" value="{{$reporte->telefono}}">
											</div>
											<label for="inputServicio" class="col-sm-2 control-label">Servicio:</label>
											<div class="col-sm-4">
												<input type="text" name="servicio" id="inputServicio" class="form-control" readonly="readonly" value="{{$reporte->ubicacion}}">
											</div>
										</div>
										<div class="form-group">
											<label for="inputTecnico" class="col-sm-2 control-label">Tecnico:</label>
											<div class="col-sm-6">
												{!! Form::select('tecnico', $tecnicos, $reporte->id_tecnico_atencion, ['class'=>'form-control','id'=>'inputTecnico']) !!}
											</div>
										</div>
										<div class="form-group">
											<label for="textareaReporte" class="col-sm-2 control-label">Reporte:</label>
											<div class="col-sm-10">
												<textarea name="reporte" id="textareaReporte" class="form-control" rows="3" readonly="readonly">{{$reporte->falla_usuario}}</textarea>
											</div>
										</div>
										<div class="form-group">
											<label for="textareaConclusion" class="col-sm-2 control-label">Conclusion:</label>
											<div class="col-sm-10">
												<textarea name="conclusion" id="textareaConclusion" class="form-control" rows="3" required="required" readonly="readonly">{{$reporte->falla_tecnico}}</textarea>
											</div>
										</div>
										<div class="form-group">
											<label for="textareaObservaciones" class="col-sm-2 control-label">Observaciones:</label>
											<div class="col-sm-10">
												<textarea name="observaciones" id="textareaObservaciones" class="form-control" rows="3" readonly="readonly">{{$reporte->observaciones}}</textarea>
											</div>
										</div>
									</div>
								</div>
								{!! Form::close() !!}
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<form class="form-horizontal" role="form">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Tiempos de Atención</h3>
										</div>
										<div class="panel-body">
											<div class="form-group">
												<label for="inputFecha_captura" class="col-sm-2 control-label">Fecha de captura:</label>
												<div class="col-sm-10">
													<input type="text" name="fecha_captura" id="inputFecha_captura" class="form-control" value="{{$reporte->fecha_alta}}"  readonly="readonly">
												</div>
											</div>
											<div class="form-group">
												<label for="inputFecha_inicio" class="col-sm-2 control-label">Fecha de inicio de Atención:</label>
												<div class="col-sm-10">
													<input type="text" name="fecha_inicio" id="inputFecha_inicio" class="form-control" value="{{$reporte->fecha_inicio}}" readonly="readonly">
												</div>
											</div>
											<div class="form-group">
												<label for="inputFecha_interrupcion" class="col-sm-2 control-label">Fecha de interrupción:</label>
												<div class="col-sm-10">
													<input type="text" name="fecha_interrupcion" id="inputFecha_interrupcion" class="form-control" value="{{$reporte->fecha_pausa}}" readonly="readonly">
												</div>
											</div>
											<div class="form-group">
												<label for="inputFecha_retiro" class="col-sm-2 control-label">Fecha de retiro de equipo:</label>
												<div class="col-sm-10">
													<input type="text" name="fecha_retiro" id="inputFecha_retiro" class="form-control" value="{{$reporte->fecha_retiro}}" readonly="readonly">
												</div>
											</div>
											<div class="form-group">
												<label for="inputFecha_reanudacion" class="col-sm-2 control-label">Fecha de reanudacion:</label>
												<div class="col-sm-10">
													<input type="text" name="fecha_reanudacion" id="inputFecha_reanudacion" class="form-control" value="{{$reporte->fecha_reanudacion}}" readonly="readonly">
												</div>
											</div>
											<div class="form-group">
												<label for="inputFecha_fin" class="col-sm-2 control-label">Fecha de finalización:</label>
												<div class="col-sm-10">
													<input type="text" name="fecha_fin" id="inputFecha_fin" class="form-control" value="{{$reporte->fecha_fin}}" readonly="">
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						@if ($reporte->id_equipo!=0)
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">
											Datos del Equipo
										</h3>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-md-8">
												<div class="row">
													<div class="col-md-6 text-primary">Marca</div>
													<div class="col-md-6">{{$equipo->marca->marca}}</div>
												</div>
												<div class="row">
													<div class="col-md-6 text-primary">Modelo</div>
													<div class="col-md-6">{{$equipo->modelo}}</div>
												</div>
												<div class="row">
													<div class="col-md-6 text-primary">Tipo</div>
													<div class="col-md-6">{{$equipo->periferico->tipo}}</div>
												</div>
												<div class="row">
													<div class="col-md-6 text-primary">Numero de serie</div>
													<div class="col-md-6">{{$equipo->serie}}</div>
												</div>
												<div class="row">
													<div class="col-md-6 text-primary">Numero de Inventario</div>
													<div class="col-md-6">{{$equipo->inventario}}</div>
												</div>
												<div class="row">
													<div class="col-md-6 text-primary">Tipo de Adquisición</div>
													<div class="col-md-6">{{$equipo->adquisicion->tipo}}</div>
												</div>
												<div class="row">
													<div class="col-md-6 text-primary">Proveedor</div>
													<div class="col-md-6">{{$equipo->otorgante->razonSocial}}</div>
												</div>
												<div class="row">
													<div class="col-md-6">Costo</div>
													<div class="col-md-6">{{$equipo->costo}}</div>
												</div>
												<div class="row">
													<div class="col-md-6">Dirección IP</div>
													<div class="col-md-6">{{$equipo->direccion_ip}}</div>
												</div>
												<div class="row">
													<div class="col-md-6">Observaciones</div>
													<div class="col-md-6">{{$equipo->observaciones}}</div>
												</div>
											</div>
										</div>	
									</div>
								</div>
							</div>
						</div>
						@endif
					</div>
					<div class="tab-pane" id="panel-seguimiento">
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-12">
									<h3>
										Avances de Reporte
									</h3>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="btn-group">
										@if($reporte->estatus != 30)
										@if ($reporte->estatus !=25)
										@if ($reporte->id_equipo == 0)
										<button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal-agregar-equipo">
											<span class="fa fa-desktop"></span> Ingresar Equipo
										</button> 
										@endif
										<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-nuevo-avance">
											<span class="glyphicon glyphicon-plus"></span> Agregar Avance
										</button>
										
										<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-interrupcion">
											<span class="glyphicon glyphicon-pause"></span>
											Aplicar Suspensión</button>
											<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-retirar-equipo">
												<span class="glyphicon glyphicon-share-alt"></span>
												Retirar Equipo
											</button>
											@else
											<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-reanudar-reporte">
												<span class="glyphicon glyphicon-play"></span>
												Reanudar Atención</button>
												@endif

												@endif
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<table class="table">
												<thead>
													<tr>
														<th>
															Fecha
														</th>
														<th>
															Avance
														</th>
														<th>
															Accion
														</th>
														<th>
															Tecnico
														</th>
														<th>
															Estatus
														</th>
													</tr>
												</thead>
												<tbody>
													@foreach ($movimientos as $row)
													<tr>
														<td>
															{{$row->fecha}}
														</td>
														<td>
															{{$row->incidencia}}
														</td>
														<td>{{$row->accion->nombre}}</td>
														<td>
															{{$row->tecnico->name}}
														</td>
														<td>
															{{$row->estado->descripcion}}
														</td>
													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="modal-nuevo-avance">
				<div class="modal-dialog">
					<div class="modal-content">
						{!! Form::open(['url'=>'/incidencia/nuevo_avance','method'=>'post','class'=>'form-horizontal']) !!}
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Nuevo Avance</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label for="inputAcciones" class="col-sm-2 control-label">Acciones:</label>
								<div class="col-sm-10">
									{!! Form::select('acciones', array('0'=>'Ninguno')+$acciones, 0, ['class'=>'form-control','id'=>'inputAcciones','required'=>'required']) !!}
								</div>
							</div>
							<div class="form-group">
								<label for="textareaAvance" class="col-sm-2 control-label">Avance:</label>
								<div class="col-sm-10">
									<textarea name="avance" id="textareaAvance" class="form-control" rows="3" required="required"></textarea>
									<input type="hidden" name="idReporte" id="inputIdReporte" class="form-control" value="{{$reporte->id}}">
								</div>
							</div>
							<div class="form-group">
								<label for="inputEstatus" class="col-sm-2 control-label">Estatus:</label>
								<div class="col-sm-10">
									{!! Form::select('estatus', $estatus, $reporte->estatus, ['class'=>'form-control','id'=>'inputEstatus','required'=>'required']) !!}
								</div>
							</div>
							<div class="form-group">
								<label for="inputResultado" class="col-sm-2 control-label">Resultado:</label>
								<div class="col-sm-10">
									{!! Form::select('resultado', array('0'=>'Ninguno')+$resultados, 0, ['class'=>'form-control','id'=>'inputResultado','required'=>'required']) !!}
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							<button type="submit" class="btn btn-primary">Guardar</button>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
			<div class="modal fade" id="modal-agregar-equipo">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						{!! Form::open(['url'=>'/incidencia/equipo','method'=>'post','class'=>'form-horizontal']) !!}
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Agregar Equipo a Reporte</h4>
						</div>
						<div class="modal-body">
							<table class="table table-hover table-condensed table-striped data-table table-condesed">
								<thead>
									<tr>
										<th>Agregar</th>
										<th>Equipo</th>
										<th>Marca</th>
										<th>Modelo</th>
										<th>Serie</th>
										<th>Inventario</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($equipos as $row)
									<?php $equipo=$row->equipos; ?>
									<tr>
										<td>{!! Form::radio('equipo', $equipo->id, false, ['class'=>'form-control']) !!}</td>
										<td>{{$equipo->periferico->tipo}}</td>
										<td>{{$equipo->marca->marca}}</td>
										<td>{{$equipo->modelo}}</td>
										<td>{{$equipo->serie}}</td>
										<td>{{$equipo->inventario}}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<div class="modal-footer">
							<input type="hidden" name="idReporte" id="inputIdReporte" class="form-control" value="{{$reporte->id}}">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							<button type="submit" class="btn btn-primary">Agregar</button>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>

			<div class="modal fade" id="modal-retirar-equipo">
				<div class="modal-dialog">
					<div class="modal-content">
						{!! Form::open(['url'=>'/incidencias/equipo/retiro','method'=>'post','class'=>'form-horizontal']) !!}
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Retirar Equipo</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label for="inputAcciones1" class="col-sm-2 control-label">Acciones:</label>
								<div class="col-sm-10">
									{!! Form::select('acciones', array('0'=>'Ninguno')+$acciones, 0, ['class'=>'form-control','id'=>'inputAcciones1','required'=>'required']) !!}
								</div>
							</div>
							<label for="textareaIncidencia" class="col-sm-2 control-label">Incidencia:</label>
							<div class="col-sm-10">
								<textarea name="incidencia" id="textareaIncidencia1" class="form-control" rows="3" required="required"></textarea>
							</div>
							
							<div class="form-group">
								<label for="inputUbicacion" class="col-sm-2 control-label">Ubicacion:</label>
								<div class="col-sm-10">
									{!! Form::select('ubicacion', $ubicaciones, 0, ['clas'=>'form-control','id'=>'inputUbicacion']) !!}
								</div>
							</div>
							<div class="form-group">
								<label for="inputEstatus" class="col-sm-2 control-label">Estatus:</label>
								<div class="col-sm-10">
									{!! Form::select('estatus', $estatus, $reporte->estatus, ['class'=>'form-control','id'=>'inputEstatus','required'=>'required']) !!}
								</div>
							</div>

						</div>
						<div class="modal-footer">
							<input type="hidden" name="idReporte" value="{{$reporte->id}}">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							<button type="submit" class="btn btn-primary">Agregar</button>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>

			<div class="modal fade" id="modal-interrupcion">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						{!! Form::open(['url'=>'incidencias/interrupcion','class'=>'form-horizontal','method'=>'post']) !!}
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Interrupción de reporte</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label for="textareaMotivo" class="col-sm-2 control-label">Motivo:</label>
								<div class="col-sm-10">
									<textarea name="motivo" id="textareaMotivo" class="form-control" rows="3" required="required"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="inputEnvioSoporte" class="col-sm-2 control-label">Envio a Soporte:</label>
								<div class="col-sm-2">
									<select name="envioSoporte" id="inputEnvioSoporte" class="form-control" required="required">
										<option value="1">Si</option>
										<option value="0">No</option>}
									</select>
								</div>
								<label for="inputProveedor" class="col-sm-2 control-label">Proveedor:</label>
								<div class="col-sm-6">
									{!! Form::select('proveedor', $proveedores, 0, ['class'=>'form-control','id'=>'inputProveedor']) !!}
								</div>
							</div>
							<div class="form-group">
								<label for="inputFolioSoporte" class="col-sm-2 control-label">Folio del Soporte:</label>
								<div class="col-sm-10">
									<input type="text" name="folioSoporte" id="inputFolioSoporte" class="form-control"  >
								</div>
							</div>
							<div class="form-group">
								<label for="inputUbicacion" class="col-sm-2 control-label">Ubicacion:</label>
								<div class="col-sm-10">
									{!! Form::select('ubicacion', $ubicaciones, $reporte->id_ubicacion, ['id'=>'inputUbicacion','class'=>'form-control']) !!}
								</div>
								<input type="hidden" name="estatus" id="inputEstatus" class="form-control" value="25">
								<input type="hidden" name="idReporte" id="inputIdReporte" class="form-control" value="{{$reporte->id}}">
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-pause"></span> Aplicar Interrupción</button>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
			
			<div class="modal fade" id="modal-reanudar-reporte">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						{!! Form::open(['url'=>'/incidencia/nuevo_avance','method'=>'post','class'=>'form-horizontal']) !!}
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Reanudar Reporte</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label for="inputAcciones1" class="col-sm-2 control-label">Acciones:</label>
								<div class="col-sm-10">
									{!! Form::select('acciones', array('0'=>'Ninguno')+$acciones, 0, ['class'=>'form-control','id'=>'inputAcciones1','required'=>'required']) !!}
								</div>
							</div>
							<label for="textareaIncidencia" class="col-sm-2 control-label">Avance:</label>
							<div class="col-sm-10">
								<textarea name="avance" id="textareaIncidencia1" class="form-control" rows="3" required="required"></textarea>
							</div>
							
							<div class="form-group">
								<label for="inputUbicacion" class="col-sm-2 control-label">Ubicacion:</label>
								<div class="col-sm-10">
									{!! Form::select('ubicacion', $ubicaciones, 0, ['class'=>'form-control','id'=>'inputUbicacion']) !!}
								</div>
							</div>
							<div class="form-group">
								<label for="inputEstatus" class="col-sm-2 control-label">Estatus:</label>
								<div class="col-sm-10">
									{!! Form::select('estatus', $estatus, 35, ['class'=>'form-control','id'=>'inputEstatus','required'=>'required']) !!}
								</div>
							</div>
							<div class="form-group">
								<label for="inputResultado" class="col-sm-2 control-label">Resultado:</label>
								<div class="col-sm-10">
									{!! Form::select('resultado', array('0'=>'Ninguno')+$resultados, 0, ['class'=>'form-control','id'=>'inputResultado','required'=>'required']) !!}
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" name="idReporte" value="{{$reporte->id}}">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							<button type="submit" class="btn btn-primary">Reiniciar reporte</button>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
			@stop