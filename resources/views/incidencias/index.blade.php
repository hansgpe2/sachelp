@extends('app')
@section('content')
<div class="page-title">
	<ol class="breadcrumb">
		<li>
			<a href="{{ url('/') }}">Principal</a>
		</li>
		<li class="active">Reportes</li>
	</ol>
	<div class="title_left">
		<h3>Control de Incidencias</h3>
	</div>
	<div class="title_right">
		<a class="btn btn-success" href='{{ url('/incidencias/nuevo') }}'>Nuevo Reporte</a>
	</div>
</div>
<div class="row clearfix">
	<table class="table table-striped table-hover" id="data-table">
		<thead>
			<tr>
				<th>Acciones</th>
				<th>Folio</th>
				<th>Fecha de alta</th>
				<th>Empleado</th>
				<th>Servicio</th>
				<th>Reporte</th>
				<th>Estatus</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($incidencias as $row)
			<tr>
				<td class="btn-group">
					<a href="{{ url('/incidencia/'.$row->id) }}" class="btn btn-success">Modificar Datos</a>
				</td>
				<td>{{ $row->id }}</td>
				<td>{{ $row->fecha_alta }}</td>
				<td>{{ $row->empleado->nombre.' '.$row->empleado->ap_paterno.' '.$row->empleado->ap_materno }}</td>
				<td>{{ $row->ubicacion }}</td>
				<td>{{ $row->falla_usuario }}</td>
				<td>{{ $row->estado->descripcion }}</td>				
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@stop