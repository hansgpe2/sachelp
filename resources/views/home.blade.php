@extends('app')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12 center-block"><img src="{{ asset('images/LogoHRVGF.jpg') }}" alt="" class="img-responsive center-block">
    <br>
    <h1>{{Config::get('unidad')}}</h1>
    </div>
  </div>
</div>
@endsection
