<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script src="{{ asset('js/jquery-2.2.0.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/jquery-barcode.min.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#codigo").barcode('{{$equipo->id}}','codabar');
		});
	</script>
	<style type="text/css">
	body
	{
		font-size: 12px;
	}
	table
	{
		width: 220px;
	}
		.logo
		{
			width: 25px;
			height: 25px;
		}
		.codigo
		{
			text-align: center;
		}
	</style>
</head>
<body>
<table>
	<tr>
	<th>
		<img src="{{ asset('images/logoEncabezado.jpg') }}" alt="" class="logo">	
	</th>
		<th colspan="2">{{Config::get('app.dependencia')}}</th>
	</tr>
	<tr>
		<th colspan="2">{{Config::get('app.unidad')}}</th>
	</tr>
	<tr>
		<th colspan="2">Control de Inventario de Equipos</th>
	</tr>
	<tr>
		<th id="codigo"></th>
	</tr>
</table>

</body>
</html>