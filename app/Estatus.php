<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Estatus extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sisestatus';

}
