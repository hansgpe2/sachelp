<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleados extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'empleados';

	/**
	 * Empleados has many Equipos.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function equipos()
	{
		// hasMany(RelatedModel, foreignKeyOnRelatedModel = empleados_id, localKey = id)
		return $this->hasMany('App\EquiposUsuarios','id_empleado','id')
		->with('equipos');
	}

	/**
	 * Empleados has one Servicio.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function servicio()
	{
		// hasOne(RelatedModel, foreignKeyOnRelatedModel = empleados_id, localKey = id)
		return $this->hasOne('App\Servicios','id','id_servicio');
	}

}
