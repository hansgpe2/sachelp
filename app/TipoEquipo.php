<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoEquipo extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tipo_equipos';

}
