<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EquiposUsuarios extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'equipo_usuario';

	/**
	 * EquiposUsuarios has one Equipos.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function equipos()
	{
		// hasOne(RelatedModel, foreignKeyOnRelatedModel = equiposUsuarios_id, localKey = id)
		return $this->hasOne('App\Equipos','id','id_equipo');
	}

	/**
	 * EquiposUsuarios has one Empleados.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function empleado()
	{
		// hasOne(RelatedModel, foreignKeyOnRelatedModel = equiposUsuarios_id, localKey = id)
		return $this->hasOne('App\Empleados','id_empleado','id');
	}

}
