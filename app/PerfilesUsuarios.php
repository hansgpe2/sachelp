<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PerfilesUsuarios extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'perfiles_usuarios';


	/**
	 * PerfilesUsuarios has one Perfiles.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function perfiles()
	{
		// hasOne(RelatedModel, foreignKeyOnRelatedModel = perfilesUsuarios_id, localKey = id)
		return $this->hasOne('App\Perfiles','id','id_perfil');
	}

}
