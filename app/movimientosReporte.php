<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class movimientosReporte extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'movimientos_reportes';

	/**
	 * movimientosReporte has one Estatus.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function estado()
	{
		// hasOne(RelatedModel, foreignKeyOnRelatedModel = movimientosReporte_id, localKey = id)
		return $this->hasOne('App\Estatus','estatus','estatus');
	}

	/**
	 * movimientosReporte has one Tecnico.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function tecnico()
	{
		// hasOne(RelatedModel, foreignKeyOnRelatedModel = movimientosReporte_id, localKey = id)
		return $this->hasOne('App\User','id','id_tecnico');
	}

	/**
	 * movimientosReporte has one Accion.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function accion()
	{
		// hasOne(RelatedModel, foreignKeyOnRelatedModel = movimientosReporte_id, localKey = id)
		return $this->hasOne('App\Acciones','id','id_acciones');
	}

	/**
	 * movimientosReporte has one Resultado.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function resultado()
	{
		// hasOne(RelatedModel, foreignKeyOnRelatedModel = movimientosReporte_id, localKey = id)
		return $this->hasOne('App\Resultados','id','id_resultado');
	}

}
