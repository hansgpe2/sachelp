<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Reportes extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'reportes';

	/**
	 * Reportes has one Empleado.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function empleado()
	{
		// hasOne(RelatedModel, foreignKeyOnRelatedModel = reportes_id, localKey = id)
		return $this->hasOne('App\Empleados','id','id_empleado');
	}

	/**
	 * Reportes has one Estatus.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function estado()
	{
		// hasOne(RelatedModel, foreignKeyOnRelatedModel = reportes_id, localKey = id)
		return $this->hasOne('App\Estatus','estatus','estatus');
	}


}
