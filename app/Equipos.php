<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipos extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'equipos';

	/**
	 * Equipos has one Marca.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function marca()
	{
		// hasOne(RelatedModel, foreignKeyOnRelatedModel = equipos_id, localKey = id)
		return $this->hasOne('App\marcas','id','id_marca');
	}

	/**
	 * Equipos has one Tipo_equipo.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function periferico()
	{
		// hasOne(RelatedModel, foreignKeyOnRelatedModel = equipos_id, localKey = id)
		return $this->hasOne('App\TipoEquipo','id','id_periferico');
	}

	/**
	 * Equipos has one Adquisiciones.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function adquisicion()
	{
		// hasOne(RelatedModel, foreignKeyOnRelatedModel = equipos_id, localKey = id)
		return $this->hasOne('App\Adquicisiones','id','id_adquisicion');
	}

	/**
	 * Equipos has one Empleado.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function Empleado()
	{
		// hasOne(RelatedModel, foreignKeyOnRelatedModel = equipos_id, localKey = id)
		return $this->hasOne('App\EquiposUsuarios','id_equipo','id')->with('empleado');
	}

	/**
	 * Equipos has one Otorgante.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function otorgante()
	{
		// hasOne(RelatedModel, foreignKeyOnRelatedModel = equipos_id, localKey = id)
		return $this->hasOne('App\Otorgantes','id','id_otorgante');
	}

}
