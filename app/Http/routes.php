<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

    Route::get('auth/register', 'Auth\AuthController@getRegister');
	Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::group(['middleware' => 'auth'], function() {

	/*Catalogo de marcas*/
    Route::get('/marcas', 'MarcasController@index');
    Route::post('/nueva_marca', 'MarcasController@create');
    Route::get('/marcas/destroy/{id}', 'MarcasController@destroy');
    Route::get('/marca/{id}', 'MarcasController@show');
    Route::post('/marca_update', 'MarcasController@update');

    /*Catalogo de perifericos*/

    Route::get('/perifericos', 'PerifericosController@index');
    Route::post('/nuevo_periferico', 'PerifericosController@create');
    Route::get('/perifericos/destroy/{id}', 'PerifericosController@destroy');
    Route::get('/perifericos/{id}', 'PerifericosController@show');
    Route::post('/perifericos_update', 'PerifericosController@update');

    /*Catalogo de Proveedores*/

    Route::get('/otorgantes', 'OtorgantesController@index');
    Route::get('/otorgante/{id}', 'OtorgantesController@show');
    Route::post('/otorgantes/nuevo', 'OtorgantesController@create');
    Route::post('/otorgantes/modificar', 'OtorgantesController@update');
    Route::get('/otorgantes/eliminar/{id}', 'OtorgantesController@destroy');

    /*Catalogo de Servicios*/

    Route::get('/servicios', 'ServiciosController@index');
    Route::get('/servicio/{id}', 'ServiciosController@show');
    Route::post('/servicios/nuevo', 'ServiciosController@create');
    Route::post('/servicios/modificar', 'ServiciosController@update');
    Route::get('/servicios/eliminar/{id}', 'ServiciosController@destroy');

    /*Catalogo de Capturistas*/

    Route::get('/capturistas', 'CapturistasController@index');
    Route::get('/capturista/{id}', 'CapturistasController@show');
    Route::post('/capturistas/nuevo', 'CapturistasController@create');
    Route::post('/capturistas/modificar', 'CapturistasController@update');
    Route::get('/capturistas/eliminar/{id}', 'CapturistasController@destroy');

    /*Control de Usuarios*/
    Route::get('users/', 'UsuariosController@index');
    Route::get('users/{id}', 'UsuariosController@show');
    Route::get('usuarios/nuevo', 'UsuariosController@create');
    Route::post('users/crear', 'UsuariosController@store');
    Route::post('users/cambiarAcceso', 'UsuariosController@update');
    Route::post('users/cambiarDatos', 'UsuariosController@edit');
    Route::post('users/delete', 'UsuariosController@destroy');

    /*Control de Empleados*/
    Route::get('/empleados', 'EmpleadosController@index');
    Route::post('/empleados/nuevo', 'EmpleadosController@create');
    Route::get('/empleado/{id}', 'EmpleadosController@show');
    Route::post('/empleados/update', 'EmpleadosController@update');
    Route::get('/empleados/eliminar/{id}', 'EmpleadosController@delete');
    Route::get('/empleados/listado','EmpleadosController@lista');
    
    Route::get('/ajax/empleado/{id}', 'EmpleadosController@empleadoAjax');

    /*Control de equipos*/
    Route::get('/equipos', 'EquiposController@index');
    Route::post('equipos/nuevo', 'EquiposController@create');
    Route::get('equipo/{id}', 'EquiposController@show');
    Route::post('equipos/update', 'EquiposController@update');
    Route::post('equipos/baja', 'EquiposController@destroy');
    Route::get('/equipos/asignar_equipo/{id}', 'EquiposController@asignacion_empleado');
    Route::post('/equipos/asignar_equipo', 'EquiposController@asignarEquipos');
    Route::get('/equipos/codigoBarras/{id}', 'EquiposController@codigo_barras');

    /*Control de Asignacion de equipo*/
    Route::get('/equipos/asignar_empleados/', 'EquiposController@asignacion_empleados');

    /*Control de Incidencias*/
    Route::get('/incidencias', 'IncidenciasController@index');
    Route::get('/incidencias/nuevo', 'IncidenciasController@create');
    Route::post('/incidencias/nuevo','IncidenciasController@store');
    Route::get('/incidencia/{id}', 'IncidenciasController@show');




});