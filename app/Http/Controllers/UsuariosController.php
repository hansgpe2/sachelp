<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;
use App\User;
use App\PerfilesUsuarios;
use App\Permisos;
use App\Perfiles;

class UsuariosController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['usuarios']=User::with('perfiles')->get();
		return view('usuarios.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['perfiles']=Perfiles::get()->lists('perfil','id');
		return view('usuarios.nuevo', $data);
	}

	public function validator(array $data)
	{
		return Validator::make($data,[
			'name'=>'required',
			'email'=>'required|email|unique:users',
			'password' => 'required|confirmed|min:6',
			'perfil'=>'required'
			]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$data=$request->all();
		$validator=$this->validator($data);
		if($validator->fails())
		{
			Session::flash('error',$validator->error());
			return redirect()->back()->withErrors($validator);
		}
		else
		{
			$usuario=new User();
			$usuario->name=$data['name'];
			$usuario->email=$data['email'];
			$usuario->password=bcrypt($data['password']);
			$usuario->password_string=$data['password'];
			$usuario->activo=1;
			$usuario->save();
			$id=$usuario->id;
			$perfilUsuario=new PerfilesUsuarios();
			$perfilUsuario->id_perfil=$data['perfil'];
			$perfilUsuario->id_usuario=$id;
			$perfilUsuario->save();
			Session::flash('mensaje','Usuario Creado');
			return redirect('users');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['usuario']=User::where('id',$id)
		->with('perfiles')->first();
		$data['perfiles']=Perfiles::all()->lists('perfil','id');
		return view('usuarios.datos', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Request $request)
	{
		$datos=$request->all();
		$rules=[
		'nombre'=>'required'
		];
		$id=$datos['id'];
		$validator=Validator::make($datos,$rules);
		if ($validator->fails()) {
			Session::flash('error',$validator->errors());
			return redirect()->back()->withErrors($validator);
		} else {
			Session::flash('mensaje','Datos Actualizados');
			$usuario=User::find($id);
			$usuario->name=$datos['nombre'];
			$usuario->save();
			$perfil=PerfilesUsuarios::where('id_usuario',$id)->first();
			$perfil->id_perfil=$datos['perfil'];
			$perfil->save();
			return redirect('users/'.$id);
		}
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$datos=$request->all();
		$id=$datos['idUsuario'];
		$rules=[
			'name'=>'required',
			'email'=>'required|email|unique:users',
			'password' => 'required|min:6'			
			];
			$validator=Validator::make($datos,$rules);
		if ($validator->fails()) {
			Session::flash('error',$validator->errors());
			return redirect()->back()->withErrors($validator);
		} else {
			$usuario=User::find($id);
			$usuario->email=$datos['login'];
			$usuario->password=bcrypt($datos['password']);
			$usuario->save();
			return redirect('users5/'.$id);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request)
	{
		$datos=$request->all();
		$usuario=User::find($datos['idUsuario']);
		$usuario->activo=$datos['activo'];
		$usuario->save();
		return redirect('users/'.$datos['idUsuario']);
	}

}
