<?php namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use App\Otorgantes;
use App\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;

class OtorgantesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data['otorgantes'] = Otorgantes::all();
        return view('otorgantes.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $datos = $request->all();
        $rules = array(
            'nombre'      => 'required',
            'razonSocial' => 'required',
            'rfc'         => 'required',
        );
        $validator = Validator::make($datos, $rules);
        if ($validator->fails()) {
            Session::flash('error', $validator->errors);
            return redirect()->back()->withErrors($validator);
        } else {
        	$datos_actuales=array('nombre'=>'');
        	$this->log($datos_actuales,$datos);
            $otorgante              = new Otorgantes();
            $otorgante->nombre      = $datos['nombre'];
            $otorgante->rfc         = $datos['rfc'];
            $otorgante->razonSocial = $datos['razonSocial'];
            $otorgante->save();
            return redirect('otorgantes');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $otorgante = Otorgantes::find($id);
        return response()->json($otorgante);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request)
    {
        $datos = $request->all();
        $id    = $datos['id'];
        $rules = array(
            'nombre'      => 'required',
            'razonSocial' => 'required',
            'rfc'         => 'required',
        );
        $validator = Validator::make($datos, $rules);
        if ($validator->fails()) {
            Session::flash('error', $validator->errors);
            return redirect()->back()->withErrors($validator);
        } else {
            $otorgante              = Otorgantes::find($id);
            $this->log($otorgante,$datos);
            $otorgante->nombre      = $datos['nombre'];
            $otorgante->rfc         = $datos['rfc'];
            $otorgante->razonSocial = $datos['razonSocial'];
            $otorgante->save();
            return redirect('otorgantes');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
    	$otorgante=Otorgantes::find($id);
    	$datos_nuevos['nombre']='eliminado';
    	$this->log($otorgante,$datos_nuevos);
    	$otorgante->delete();
    	return redirect('otorgantes');
    }

    public function log($datos_actuales, $datos_nuevos)
    {
        $datos_nuevos = json_encode($datos_nuevos);

        $logs                 = new Log();
        $logs->datos_actuales = json_encode($datos_actuales);
        $logs->datos_nuevos   = $datos_nuevos;
        $logs->ip             = $_SERVER['REMOTE_ADDR'];
        $logs->id_usuario     = Auth::id();
        $logs->catalogo       = 'otorgantes';
        $logs->save();
    }

}
