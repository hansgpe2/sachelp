<?php namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Session;

use Illuminate\Http\Request;
use App\marcas;
use Carbon\Carbon;
use App\Log;

class MarcasController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['marcas']=marcas::all();
		return view('marcas.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$datos=$request->all();
		$rules=array(
			'marca'=>'required'
			);
		$validator=Validator::make($datos,$rules);
		if ($validator->fails()) {
			$request->session()->flash('error', $validator->messages);
			return redirect()->back()->withErrors($validator);
		}
		else
		{
			Session::flash('mensaje','Marca insertada');
			$datos_actuales=array('marca'=>'');
			
			$marca=new marcas();
			$marca->marca=$datos['marca'];
			$marca->save();
			$id=$marca->id;
			$this->Log($datos_actuales,$id);

			return redirect('marcas');
		}

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$marca=marcas::find($id);
		return response()->json($marca);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Request request
	 * @return Response
	 */
	public function update(Request $request)
	{
		$datos=$request->all();
		$id=$datos['id'];
		$rules=array(
			'marca'=>'required'
			);
		$validator=Validator::make($datos,$rules);
		if ($validator->fails()) {
			$request->session()->flash('error', $validator->messages);
			return redirect()->back()->withErrors($validator);
		}
		else
		{
			Session::flash('mensaje','Marca modificada');
			$marca=marcas::find($id);
			$datos_actuales=$marca;
			$marca->marca=$datos['marca'];
			$marca->save();
			$this->Log($datos_actuales,$datos);
			return redirect('/marcas');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Session::flash('mensaje','Marca eliminada');
		$marca=marcas::find($id);
		$this->log($marca,['marca'=>'eliminada']);
		$marca->delete();
		return redirect('/marcas');

	}

	public function log($datos_actuales,$datos_nuevos)
	{
		$datos_nuevos=json_encode($datos_nuevos);
		

		$logs=new Log();
		$logs->datos_actuales=json_encode($datos_actuales);
		$logs->datos_nuevos=$datos_nuevos;
		$logs->ip=$_SERVER['REMOTE_ADDR'];
		$logs->id_usuario=Auth::id();
		$logs->catalogo='marca';
		$logs->save();
	}

}
