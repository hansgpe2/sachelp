<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use Auth;
use Carbon\Carbon;

use App\Equipos;
use App\TipoEquipo;
use App\marcas;
use App\Otorgantes;
use App\Empleados;
use App\Adquicisiones;

use App\EquiposUsuarios;
use App\MovimientosEquipo;
use App\Log;



class EquiposController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['equipos']=Equipos::
		with('marca')
		->with('periferico')
		->with('otorgante')
		->with('adquisicion')
		->get();
		$data['tipos']=TipoEquipo::all()->lists('tipo','id');
		$data['marcas']=marcas::all()->lists('marca','id');
		$data['adquicision']=Adquicisiones::all()->lists('tipo','id');
		$data['proveedor']=Otorgantes::all()->lists('nombre','id');

		return view('equipos.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$datos=$request->all();
		$validator=$this->validator($datos);
		if ($validator->fails()) {
			Session::flash('error',$validator->errors());
			return redirect()->back()->withErrors($validator);
		} else {
			$equipo=new Equipos();
			$equipo->serie=$datos['serie'];
			$equipo->inventario=$datos['inventario'];
			$equipo->id_marca=$datos['marca'];
			$equipo->modelo=$datos['modelo'];
			$equipo->fecha_alta=$datos['fecha_alta'];
			$equipo->id_adquisicion=$datos['adquicision'];
			$equipo->id_otorgante=$datos['proveedor'];
			$equipo->observaciones=$datos['observaciones'];
			$equipo->id_periferico=$datos['tipo'];
			$equipo->costo=$datos['costo'];
			$equipo->direccion_ip=$datos['direccion_ip'];
			$equipo->save();
			$id=$equipo->id;
			return redirect('equipo/'.$id);
		}
		

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['equipo']=Equipos::where('id',$id)->with('periferico')
		->with('marca')
		->with('adquisicion')
		->with('otorgante')
		->first();
		
		$data['tipos']=TipoEquipo::all()->lists('tipo','id');
		$data['marcas']=marcas::all()->lists('marca','id');
		$data['adquicision']=Adquicisiones::all()->lists('tipo','id');
		$data['proveedor']=Otorgantes::all()->lists('nombre','id');
		$data['usuarios']=EquiposUsuarios::where('id_equipo')
		->with('empleado')
		->orderBy('fecha_asignacion','desc')->get();
		return view('equipos.datos_equipo', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$datos=$request->all();
		$id=$datos['idEquipo'];
		$validator=$this->validator($datos);
		if ($validator->fails()) {
			Session::flash('error',$validator->errors());
			return redirect()->back()->withErrors($validator);
		} else {
			$equipo=Equipos::find($id);
			$equipo->serie=$datos['serie'];
			$equipo->inventario=$datos['inventario'];
			$equipo->id_marca=$datos['marca'];
			$equipo->modelo=$datos['modelo'];
			$equipo->fecha_alta=$datos['fecha_alta'];
			$equipo->id_adquisicion=$datos['adquicision'];
			$equipo->id_otorgante=$datos['proveedor'];
			$equipo->observaciones=$datos['observaciones'];
			$equipo->id_periferico=$datos['tipo'];
			$equipo->costo=$datos['costo'];
			$equipo->direccion_ip=$datos['direccion_ip'];
			$equipo->save();
			
			return redirect('equipo/'.$id);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request)
	{
		$datos=$request->all();
		$id=$datos['id'];
		$equipo=Equipos::find($id);
		$equipo->activo=0;
		$equipo->observaciones=$datos['motivo'];
		$equipo->fecha_baja=Carbon::now();
		$equipo->save();
		return redirect('equipos/');
	}

	public function asignacion_empleado($idEmpleado)
	{
		$data['empleado']=Empleados::find($idEmpleado);
		$data['equipos']=Equipos::where('asignado',0)->get();
		$data['equipos_asignados']=EquiposUsuarios::where('id_empleado',$idEmpleado)->with('equipos')->get();
		return view('equipos.asignar_equipo', $data);
	}



	public function validator(array $data)
	{
		return Validator::make($data,[
			'modelo'=>'required',
			'serie'=>'required',
			'fecha_alta'=>'required',
			'direccion_ip'=>'ip'
			]);
	}

	public function asignarEquipos(Request $request)
	{
		$datos=$request->all();
		$idEmpleado=$datos['empleado'];
		$equipos=$datos['equipos'];
		foreach ($equipos as $row) {
			$asignacion=new EquiposUsuarios();
			$asignacion->id_equipo=$row;
			$asignacion->id_empleado=$idEmpleado;
			$asignacion->save();
			$equipo=Equipos::find($row);
			$datos_actuales=$equipo;
			$equipo->asignado=1;
			$equipo->save();
			$datos_equipo=[
			'id_equipo'=>$row,
			'asignado'=>1,
			'id_empleado'=>$idEmpleado];
			$this->log($datos_actuales,$datos_equipo);
		}
		return redirect('/equipos/asignar_equipo/'.$idEmpleado);
	}

	public function codigo_barras($id)
	{
		$data['equipo']=Equipos::find($id);
		return view('reportes.codigoBarras', $data);
	}

	public function log($datos_actuales, $datos_nuevos)
	{
		$datos_nuevos = json_encode($datos_nuevos);

		$logs                 = new Log();
		$logs->datos_actuales = json_encode($datos_actuales);
		$logs->datos_nuevos   = $datos_nuevos;
		$logs->ip             = $_SERVER['REMOTE_ADDR'];
		$logs->id_usuario     = Auth::id();
		$logs->catalogo       = 'equipos';
		$logs->save();
	}

	public function asignacion_empleados()
	{
		$data['empleados']=Empleados::all();
		return view('equipos.asignar_empleados', $data);
	}


}
