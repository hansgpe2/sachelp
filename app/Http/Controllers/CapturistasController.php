<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Session;
use Auth;

use Illuminate\Http\Request;

use App\Log;
use App\Capturistas;

class CapturistasController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['capturistas']=Capturistas::all();
		return view('capturistas.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$datos=$request->all();
		$rules=array(
			'nombre'=>'required'
			);
		$validator=Validator::make($datos,$rules);
		if($validator->fails())
		{
			Session::flash('error',$validator->errors);
			return redirect()->back()->withErrors($validator);
		}
		else
		{
			$datos_actuales=array('nombre'=>'');
			
			$capturista=new Capturistas();
			$capturista->nombre=$datos['nombre'];
			$capturista->save();
			$this->log($datos_actuales,$datos);
			return redirect('capturistas');
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$capturista=Capturistas::find($id);
		return response()->json($capturista);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$datos=$request->all();
		$id=$datos['id'];
		$rules=array(
			'nombre'=>'required'
			);
		$validator=Validator::make($datos,$rules);
		if($validator->fails())
		{
			Session::flash('error',$validator->errors);
			return redirect()->back()->withErrors($validator);
		}
		else
		{
			
			
			$capturista=Capturistas::find($id);
			$this->log($capturista,$datos);
			$capturista->nombre=$datos['nombre'];
			$capturista->save();
			
			return redirect('capturistas');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$datos=array('nombre'=>'eliminado');
		$capturista=Capturistas::find($id);
			$this->log($capturista,$datos);
		$capturista->delete();
		return redirect('capturistas');
	}

	public function log($datos_actuales, $datos_nuevos)
	{
		$datos_nuevos = json_encode($datos_nuevos);

		$logs                 = new Log();
		$logs->datos_actuales = json_encode($datos_actuales);
		$logs->datos_nuevos   = $datos_nuevos;
		$logs->ip             = $_SERVER['REMOTE_ADDR'];
		$logs->id_usuario     = Auth::id();
		$logs->catalogo       = 'capturistas';
		$logs->save();
	}


}
