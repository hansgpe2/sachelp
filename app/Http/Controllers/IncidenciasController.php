<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Session;
use Auth;
use Carbon\Carbon;

use Illuminate\Http\Request;

use App\Reportes;
use App\movimientosReporte;
use App\Acciones;
use App\Estatus;
use App\Resultados;
use App\User;
use App\EquiposUsuarios;
use App\Equipos;
use App\Ubicaciones;
use App\Otorgantes;


class IncidenciasController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['incidencias']=Reportes::with('empleado')
		->with('estado')
		->get();
		return view('incidencias.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('incidencias.nuevo');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{	
		$datos=$request->all();
		$rules=[
			'idEmpleado'=>'required|min:1',
			'telefono'=>'required|min:2',
			'servicio'=>'required|min:3',
			'reporte'=>'required|min:10'
		];
		$validator=Validator::make($datos,$rules);
		if ($validator->fails()) {
			Session::flash('error',$validator->errors());
			return redirect()->back()->withErrors($validator);
		} else {
			$reporte=new Reportes();
			$reporte->id_empleado=$datos['idEmpleado'];
			$reporte->telefono=$datos['telefono'];
			$reporte->ubicacion=$datos['servicio'];
			$reporte->falla_usuario=$datos['reporte'];
			$reporte->fecha_alta=Carbon::now();
			$reporte->estatus=10;
			$reporte->id_capturista=Auth::user()->id;
			$reporte->observaciones=$datos['observaciones'];
			$reporte->save();
			return redirect('/incidencias');
		}
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['reporte']=Reportes::where('id',$id)
		->with('empleado')
		->with('estado')
		->first();
		$data['acciones']=Acciones::all()->lists('nombre','id');
		$data['movimientos']=movimientosReporte::where('id_reporte',$id)
		->with('estado')
		->with('tecnico')
		->with('accion')
		->with('resultado')
		->get();
		$data['estatus']=Estatus::all()->lists('descripcion','estatus');
		$data['equipos']=EquiposUsuarios::where('id_empleado',$data['reporte']->id_empleado)->with('equipos')->get();
		$data['tecnicos']=User::with(['perfiles'=>function($query){
			return $query->where('id_perfil',2);
		}])
		->get()->lists('name','id');
		if($data['reporte']->id_equipo !=0){
				$data['equipo']=Equipos::where('id',$id)->with('periferico')
				->with('marca')
				->first();
			}
		$data['resultados']=Resultados::all()->lists('tipo','id');
		$data['ubicaciones']=Ubicaciones::all()->lists('nombre','id');
		$data['proveedores']=Otorgantes::all()->lists('razonSocial','id');
		// dd($data);
		
		return view('incidencias.avances', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id,$idTecnico)
	{
		$reporte=Reportes::find($id);
		if($reporte->id_tecnico_apertura==0){
			$reporte->id_tecnico_apertura=$idTecnico;
		}
		$reporte->id_tecnico_atencion=$idTecnico;
		$reporte->save();
		return response()->json($reporte);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$datos=$request->all();
		$rules=[
			'avance'=>'required|min:10'
		];
		$validator=Validator::make($datos,$rules);
		if ($validator->fails()) {
			Session::flash('error',$validator->errors());
			return redirect()->back()->withErrors($validator);
		} else {
			$movimiento=new movimientosReporte();
			$movimiento->incidencia=$datos['avance'];
			$movimiento->id_reporte=$datos['idReporte'];
			$movimiento->id_tecnico=Auth::user()->id;
			$movimiento->estatus=$datos['estatus'];
			$movimiento->fecha=Carbon::now();
			$movimiento->id_acciones=$datos['acciones'];
			$movimiento->id_resultado=$datos['resultado'];
			$movimiento->save();

			$reporte=Reportes::find($datos['idReporte']);
			$reporte->id_tecnico_atencion=Auth::user()->id;
			$estatus=$reporte->estatus;
			if($estatus!=$datos['estatus']){
				$reporte->estatus=$datos['estatus'];
			}
			switch ($datos['estatus']) {
				case '30':
					$reporte->fecha_fin=Carbon::now();
					$reporte->id_tecnico_cierre=Auth::user()->id;
					$reporte->falla_tecnico=$datos['avance'];
					break;
				case '25':
					$reporte->fecha_pausa=Carbon::now();
					break;
				case '35':
					$reporte->fecha_reanudacion=Carbon::now();
					break;
				case '20':
					$reporte->fecha_inicio=Carbon::now();
					break;
			}
			$reporte->save();
			return redirect('/incidencia/'.$datos['idReporte']);		
			
		}
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function agregarEquipo(Request $request)
	{
		$datos=$request->all();
		$idReporte=$datos['idReporte'];
		$reporte=Reportes::find($idReporte);
		$reporte->id_equipo=$datos['equipo'];
		$reporte->save();
		return redirect('incidencia/'.$idReporte);
	}

	public function retirarEquipo(Request $request)
	{
		$datos=$request->all();		
		$rules=[
			'incidencia'=>'required|min:10'
		];
		$validator=Validator::make($datos,$rules);
		if ($validator->fails()) {
			Session::flash('error',$validator->errors());
			return redirect()->back()->withErrors($validator);
		} else {
			$movimiento=new movimientosReporte();
			$movimiento->id_reporte=$datos['idReporte'];
			$movimiento->id_tecnico=Auth::user()->id;
			$movimiento->incidencia=$datos['incidencia'];
			$movimiento->estatus=$datos['estatus'];
			$movimiento->fecha=Carbon::now();
			$movimiento->id_acciones=$datos['acciones'];
			$movimiento->save();
			$reporte=Reportes::find($datos['idReporte']);
			$reporte->fecha_retiro=Carbon::now();
			$reporte->id_ubicacion=$datos['ubicacion'];
			$estatus=$reporte->estatus;
			if($estatus!=$datos['estatus']){
				$reporte->estatus=$datos['estatus'];
			}
			switch ($datos['estatus']) {
				case '30':
					$reporte->fecha_fin=Carbon::now();
					$reporte->id_tecnico_cierre=Auth::user()->id;
					$reporte->falla_tecnico=$datos['avance'];
					break;
				case '25':
					$reporte->fecha_pausa=Carbon::now();
					break;
				case '35':
					$reporte->fecha_reanudacion=Carbon::now();
					break;
				case '20':
					$reporte->fecha_inicio=Carbon::now();
					break;
			}
			
			$reporte->save();
			return redirect('incidencia/'.$datos['idReporte']);
		}
		
	}

	public function aplicarInterrupcion(Request $request)
	{
		$datos=$request->all();
		$rules=[
		'motivo'=>'required|min:10'
		];
		$validator=Validator::make($datos,$rules);
		if ($validator->fails()) {
			Session::flash('error',$validator->error());
			return redirect()->back()->withErrors($validator);
		} else {
			$movimiento=new movimientosReporte();
			$movimiento->id_reporte=$datos['idReporte'];
			$movimiento->id_tecnico=Auth::user()->id;
			$movimiento->incidencia=$datos['motivo'];
			$movimiento->estatus=$datos['estatus'];
			$movimiento->fecha=Carbon::now();
			$movimiento->save();
			$idPausa=$movimiento->id;
			$reporte=Reportes::find($datos['idReporte']);
			$reporte->fecha_pausa=Carbon::now();
			$reporte->id_tecnico_atencion=Auth::user()->id;
			$reporte->id_ubicacion=$datos['ubicacion'];
			$reporte->folio_externo=$datos['folioSoporte'];
			$reporte->estatus=$datos['estatus'];
			$reporte->id_pausa=$idPausa;
			$reporte->save();
			return redirect('incidencia/'.$datos['idReporte']);
		}
		
	}

}
