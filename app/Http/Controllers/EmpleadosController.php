<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use Auth;
use Carbon\Carbon;

use App\Empleados;
use App\Servicios;
use App\EquiposUsuarios;



class EmpleadosController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['empleados']=Empleados::all();
		$data['servicios']=Servicios::all()->lists('nombre','id');
		return view('empleados.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$datos=$request->all();
		$rules=[
			'paterno'=>'required|min:4',
			'materno'=>'required|min:4',
			'nombre'=>'required|min:4',
			'horarios'=>'required|min:4',
			'telefono'=>'required',
			'puesto'=>'required|min:4'
		];
		$validator=Validator::make($datos,$rules);
		if ($validator->fails()) {
			Session::flash('error',$validator->errors());
			return redirect()->back()->withErrors($validator);
		} else {
			$empleado=new Empleados();
			$empleado->ap_paterno=$datos['paterno'];
			$empleado->ap_materno=$datos['materno'];
			$empleado->nombre=$datos['nombre'];
			$empleado->puesto=$datos['puesto'];
			$empleado->telefono=$datos['telefono'];
			$empleado->id_servicio=$datos['servicio'];
			$empleado->horarios=$datos['horarios'];
			$empleado->save();
			$id=$empleado->id;
			return redirect('empleado/'.$id);
		}
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['empleado']=Empleados::find($id);
		$data['servicios']=Servicios::all()->lists('nombre','id');
		$data['equipos']=EquiposUsuarios::where('id_empleado',$id)->with('equipo')->get();
		return view('empleados.datos_empleados', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$datos=$request->all();
		$id=$datos['id'];
		$rules=[
			'paterno'=>'required|min:4',
			'materno'=>'required|min:4',
			'nombre'=>'required|min:4',
			'horarios'=>'required|min:4',
			'telefono'=>'required',
			'puesto'=>'required|min:4'
		];
		$validator=Validator::make($datos,$rules);
		if ($validator->fails()) {
			Session::flash('error',$validator->errors());
			return redirect()->back()->withErrors($validator);
		} else {
			$empleado=Empleados::find($id);
			$empleado->ap_paterno=$datos['paterno'];
			$empleado->ap_materno=$datos['materno'];
			$empleado->nombre=$datos['nombre'];
			$empleado->puesto=$datos['puesto'];
			$empleado->telefono=$datos['telefono'];
			$empleado->horarios=$datos['horarios'];
			$empleado->id_servicio=$datos['servicio'];
			$empleado->save();
			return redirect('empleado/'.$id);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$equipos=EquiposUsuarios::where('id_empleado',$id)->with('equipo')->count();
		if($equipos>0)
		{
			Session::flash('error','Reasigne los equipos antes de continuar');
			return redirect()->back();
		}
		else
		{
			$empleado=Empleados::find($id);
			$empleado->delete();
			return redirect('empleados/');
		}
	}

	public function lista(Request $request)
	{
		$datos=$request->all();
		$cons=$datos['term'];
		$empleados=Empleados::where('ap_paterno',$cons)
		->orWhere('ap_materno',$cons)
		->orWhere('nombre',$cons)
		->orWhere('num_empleado',$cons)
		->get();
		$list_empleados=[];
			
		foreach ($empleados as $empleado) {
			$list_empleados[]=['id'=>$empleado['id'],'text'=>$empleado['num_empleado'].' - '.$empleado['ap_paterno'].' '.$empleado['ap_materno'].' '.$empleado['nombre']];
		}
		return response()->json($list_empleados);
	}

	public function empleadoAjax($id)
	{
		$empleado=Empleados::where('id',$id)
		->with('servicio')
		->first();
		return response()->json($empleado);
	}

}
