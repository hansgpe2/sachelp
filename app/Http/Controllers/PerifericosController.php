<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use Auth;
use App\TipoEquipo;
use App\Log;

class PerifericosController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['perifericos']=TipoEquipo::all();
		return view('perifericos.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$datos=$request->all();
		$rules=array(
			'periferico'=>'required'
			);
		$validator=Validator::make($datos,$rules);
		if ($validator->fails()) {
			$request->session()->flash('error', $validator->messages);
			return redirect()->back()->withErrors($validator);
		}
		else
		{
			Session::flash('mensaje','Periferico insertado');
			$datos_actuales=array('marca'=>'');
			
			$periferico=new TipoEquipo();
			$periferico->tipo=$datos['periferico'];
			$periferico->save();
			
			$this->Log($datos_actuales,$datos);

			return redirect('perifericos');
			}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$periferico=TipoEquipo::find($id);
		return response()->json($periferico);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$datos=$request->all();
		$id=$datos['id'];
		$rules=array(
			'periferico'=>'required'
			);
		$validator=Validator::make($datos,$rules);
		if ($validator->fails()) {
			$request->session()->flash('error', $validator->messages);
			return redirect()->back()->withErrors($validator);
		}
		else
		{
			Session::flash('mensaje','Periferico modificado');
			$periferico=TipoEquipo::find($id);
			$this->log($periferico,$datos);
			$periferico->tipo=$datos['periferico'];
			$periferico->save();
			return redirect('perifericos');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Session::flash('mensaje','Periferico eliminado');
		$periferico=TipoEquipo::find($id);
		$this->log($periferico,['periferico'=>'X']);
		$periferico->delete();
		return redirect('/perifericos');
	}

	public function log($datos_actuales,$datos_nuevos)
	{
		$datos_nuevos=json_encode($datos_nuevos);
		

		$logs=new Log();
		$logs->datos_actuales=json_encode($datos_actuales);
		$logs->datos_nuevos=$datos_nuevos;
		$logs->ip=$_SERVER['REMOTE_ADDR'];
		$logs->id_usuario=Auth::id();
		$logs->catalogo='perifericos';
		$logs->save();
	}

}
