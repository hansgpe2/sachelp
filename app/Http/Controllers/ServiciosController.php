<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Session;
use Auth;


use Illuminate\Http\Request;
use App\Servicios;

class ServiciosController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['servicios']=Servicios::all();
		return view('servicios.index', $data);
	}

	public function validator(array $datos)
	{
		return Validator::make($datos,[
			'servicio'=>'required|min:4'
			]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$datos=$request->all();
		$validator=$this->validator($datos);
		if ($validator->fails()) {
			Session::flash('error',$validator->errors());
			return redirect()->back()->withErrors($validator);
		} else {
			$servicio=new Servicios();
			$servicio->nombre=$datos['servicio'];
			$servicio->save();
			return redirect('servicios');
		}
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data=Servicios::find($id);
		return response()->json($data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$datos=$request->all();
		$id=$datos['id'];
		$validator=$this->validator($datos);
		if ($validator->fails()) {
			Session::flash('error',$validator->errors());
			return redirect()->back()->withErrors($validator);
		} else {
			$servicios=Servicios::find($id);
			$servicio->nombre=$datos['servicio'];
			$servicio->save();
			return redirect('servicios');
		}
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$servicio=Servicios::find($id);
		$servicio->delete();
		return redirect('servicios');
	}

}
