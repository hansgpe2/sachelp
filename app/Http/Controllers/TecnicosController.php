<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Session;
use Auth;

use App\Log;
use App\Tecnicos;

use Illuminate\Http\Request;

class TecnicosController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['tecnicos']=Tecnicos::all();
		return view('tecnicos.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$datos=$request->all();
		$rules=array(
			'tecnico'=>'required'
			);
		$validator=Validator::make($datos,$rules);
		if($validator->fails())
		{
			Session::flash('error',$validator->errors);
			return redirect()->back()->withErrors($validator);
		}
		else
		{
			$datos_actuales=array('tecnico'=>'');
			
			$tecnico=new Tecnicos();
			$tecnico->nombre=$datos['tecnico'];
			$tecnico->save();
			$this->log($datos_actuales,$datos);
			return redirect('tecnicos');
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$tecnico=Tecnicos::find($id);
		return response()->json($tecnico);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		$datos=$request->all();
		$rules=array(
			'tecnico'=>'required'
			);
		$validator=Validator::make($datos,$rules);
		if($validator->fails())
		{
			Session::flash('error',$validator->errors);
			return redirect()->back()->withErrors($validator);
		}
		else
		{
			$tecnico=Tecnicos::find($datos['id']);
			$tecnico->nombre=$datos['tecnico'];
			$tecnico->save();
			$this->log($tecnico,$datos);
			return redirect('tecnicos');
		}

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$datos_nuevos=array('nombre'=>'eliminado');
		$tecnico=Tecnicos::find($id);
		$this->log($tecnico,$datos_nuevos);
		$tecnico->delete();
		return redirect('tecnicos');
	}

	public function log($datos_actuales, $datos_nuevos)
	{
		$datos_nuevos = json_encode($datos_nuevos);

		$logs                 = new Log();
		$logs->datos_actuales = json_encode($datos_actuales);
		$logs->datos_nuevos   = $datos_nuevos;
		$logs->ip             = $_SERVER['REMOTE_ADDR'];
		$logs->id_usuario     = Auth::id();
		$logs->catalogo       = 'tecnicos';
		$logs->save();
	}

}
