<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Acciones extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cat_acciones';

}
