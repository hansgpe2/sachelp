<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Otorgantes extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'otorgantes';

}
