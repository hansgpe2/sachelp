<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		/*Usuarios y perfiles*/
		DB::table('permisos')->delete();
		DB::table('permisos')->insert(array(
			array('permiso'=>'levantar_incidencias'),
			array('permiso'=>'seguimiento_incidencias'),
			array('permiso'=>'consulta_incidencias'),
			array('permiso'=>'control_equipos'),
			array('permiso'=>'consulta_equipos'),
			array('permiso'=>'catalogos'),
			array('permiso'=>'consulta_catalogos'),
			array('permiso'=>'reportes')
			));
		DB::table('perfiles')->delete();
		DB::table('perfiles')->insert(array(
			array('perfil'=>'capturista'),
			array('perfil'=>'tecnico'),
			array('perfil'=>'direccion de sistemas'),
			array('perfil'=>'administrador')
			));
		DB::table('users')->delete();
		DB::table('users')->insert(array(
			array(
				'login'=>'admin',
				'password'=>bcrypt('123456'),
				'activo'=>1
				)
			));
		DB::table('perfiles_usuarios')->delete();
		DB::table('perfiles_usuarios')->insert(array(
			array(
				'id_perfil'=>4,
				'id_usuario'=>1
				)
			));
		/*Tipo de Adquisiciones*/
		$adquisiciones=array(
			array(
			'tipo'=>'compra'
			),
			array(
			'tipo'=>'renta'
			),
			array(
			'tipo'=>'donacion'
			));
		DB::table('adquisiciones')->delete();
		DB::table('adquisiciones')
		->insert($adquisiciones);
		/*Ubicaciones de retiro*/
		$ubicaciones=array(
			array('nombre'=>'oficina'),
			array('nombre'=>'taller'),
			array('nombre'=>'externo'));
		DB::table('ubicaciones')->delete();
		DB::table('ubicaciones')
		->insert($ubicaciones);
		/*Catalogo de pausas*/
		DB::table('cat_pausas')->delete();
		DB::table('cat_pausas')
		->insert(array(
			array('tipo'=>'pieza'),
			array('tipo'=>'horario'),
			array('tipo'=>'empleado'),
			array('tipo'=>'otro')));
		/*Tabla estatus*/
		DB::table('sisEstatus')->delete();
		DB::table('sisEstatus')->insert(array(
			array('estatus'=>10,
			'descripcion'=>'captura',
			),
			array('estatus'=>20,
			'descripcion'=>'atendida',
			),
			array('estatus'=>25,
			'descripcion'=>'pausa',
			),
			array('estatus'=>30,
			'descripcion'=>'cerrado',
			),
			array('estatus'=>35,
			'descripcion'=>'reabierto',
			)));
		/*catalogo de acciones*/
		DB::table('cat_acciones')->delete();
		DB::table('cat_acciones')->insert(
			array(
			array('nombre'=>'Mantenimiento Correctivo'),
			array('nombre'=>'Soporte'),
			array('nombre'=>'Software'),
			array('nombre'=>'Instalacion'),
			array('nombre'=>'Desinstalacion'),
			array('nombre'=>'Mantenimiento Preventivo'),
			array('nombre'=>'Cableado')
			));
		/*catalogo de resultdaos*/
		DB::table('cat_resultados')->delete();
		DB::table('cat_resultados')->insert(
			array(
			array('tipo'=>'Corregido'),
			array('tipo'=>'Suspendido'),
			array('tipo'=>'Baja'),
			array('tipo'=>'Capacitado')
			)
			);

	}

}
