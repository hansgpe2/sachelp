<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEstatus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('sisEstatus')) {
		    Schema::create('sisEstatus', function (Blueprint $table) {
		        $table->increments('id');
		        $table->integer('estatus');
		        $table->string('descripcion');
		    });
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('sisEstatus');
	}

}
