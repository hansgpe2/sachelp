<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovimientosEquiposTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('movimientos_equipos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_equipo');
			$table->string('estatus');
			$table->string('encargado');
			$table->integer('id_usuario');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('movimientos_equipos');
	}

}
