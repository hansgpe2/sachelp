<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMarcas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('cat_marcas')) {
		    Schema::create('cat_marcas', function (Blueprint $table) {
		        $table->increments('id');
		        $table->string('marca');
		        $table->timestamps();
		    });
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('cat_marcas');
	}

}
