<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReportes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('reportes')) {
		    Schema::create('reportes', function (Blueprint $table) {
		        $table->bigIncrements('id');
		        $table->bigInteger('id_empleado');
		        $table->bigInteger('id_equipo');
		        $table->string('telefono');
		        $table->datetime('fecha_alta');
		        $table->datetime('fecha_inicio');
		        $table->datetime('fecha_pausa');
		        $table->datetime('fecha_retiro');
		        $table->datetime('fecha_reanudacion');
		        $table->datetime('fecha_fin');
		        $table->integer('id_pausa');
		        $table->integer('id_ubicacion');
		        $table->integer('folio_externo');
		        $table->integer('estatus');
		        $table->integer('calidad');
		        $table->text('observaciones_calidad');
		        $table->integer('id_tecnico_apertura');
		        $table->integer('id_tecnico_atencion');
		        $table->integer('id_tecnico_cierre');
		        $table->timestamps();
		        
		    });
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('reportes');
	}

}
