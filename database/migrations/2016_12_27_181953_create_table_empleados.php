<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmpleados extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('empleados')) {
		    Schema::create('empleados', function (Blueprint $table) {
		        $table->bigincrements('id');
		        $table->string('ap_paterno');
		        $table->string('ap_materno');
		        $table->string('nombre');
		        $table->string('puesto');
		        $table->integer('id_servicio');
		        $table->string('telefono');
		        $table->string('horarios');
		        $table->timestamps();
		    });
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('empleados');
	}

}
