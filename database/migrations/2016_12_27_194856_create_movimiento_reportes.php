<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovimientoReportes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('movimientos_reportes')) {
		    Schema::create('movimientos_reportes', function (Blueprint $table) {
		        $table->bigIncrements('id');
		        $table->bigInteger('id_reporte');
		        $table->integer('id_tecnico');
		        $table->text('incidencia');
		        $table->integer('estatus');
		        $table->datetime('fecha');
		        $table->integer('id_acciones');
		        $table->integer('id_resultado');
		        $table->timestamps();
		    });
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('movimientos_reportes');
	}

}
