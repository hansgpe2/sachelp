<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCpuPerifericos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('cpu_perifericos')) {
		    Schema::create('cpu_perifericos', function (Blueprint $table) {
		        $table->increments('id');
		        $table->integer('id_cpu');
		        $table->integer('id_equipo');
		        $table->timestamps();
		    });
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
