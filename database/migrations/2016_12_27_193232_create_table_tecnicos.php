<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTecnicos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('tecnicos')) {
		    Schema::create('tecnicos', function (Blueprint $table) {
		        $table->increments('id');
		        $table->string('nombre');
		        $table->timestamps();
		    });
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('tecnicos');
	}

}
