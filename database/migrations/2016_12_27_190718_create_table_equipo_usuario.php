<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEquipoUsuario extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('equipo_usuario')) {
		    Schema::create('equipo_usuario', function (Blueprint $table) {
		        $table->bigincrements('id');
		        $table->integer('id_equipo');
		        $table->integer('id_empleado');
		        $table->timestamps();
		    });
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('equipo_usuario');
	}

}
