<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEquipos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('equipos')) {
		    Schema::create('equipos', function (Blueprint $table) {
		        $table->bigincrements('id');
		        $table->string('serie');
		        $table->string('inventario');
		        $table->integer('marca');
		        $table->string('modelo');
		        $table->date('fecha_alta');
		        $table->date('fecha_baja');
		        $table->integer('id_adquisicion');
		        $table->integer('id_otorgante');
		        $table->text('observaciones');
		        $table->double('costo',15,2);
		        $table->boolean('equipo_completo');
		        $table->boolean('periferico');
		        $table->timestamps();
		    });
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('equipos');
	}

}
