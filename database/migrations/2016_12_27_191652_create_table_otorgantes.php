<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOtorgantes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('otorgantes')) {
		    Schema::create('otorgantes', function (Blueprint $table) {
		        $table->increments('id');
		        $table->string('nombre');
		        $table->string('rfc');
		        $table->string('razonSocial');
		        $table->timestamps();
		    });
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('otorgantes');
	}

}
