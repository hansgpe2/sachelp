<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoporteExterno extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('soporte_externo')) {
		    Schema::create('soporte_externo', function (Blueprint $table) {
		        $table->increments('id');
		        $table->string('nombre');
		        $table->string('razonSocial');
		        $table->string('rfc');
		        $table->string('direccion');
		        $table->timestamps();
		    });
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('soporte_externo');
	}

}
